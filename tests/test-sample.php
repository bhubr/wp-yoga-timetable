<?php
/**
 * Class SampleTest
 *
 * @package Wp_Yoga_Timetable
 */
require 'insert-v1-data.php';

/**
 * Sample test case.
 */
class SampleTest extends WP_UnitTestCase {

	/**
	 * A single example test.
	 */
	function test_sample() {
		global $wpdb;
		// Replace this with some actual testing code.
		$sql = file_get_contents(__DIR__ . '/options.sql');
		$this->assertEquals($wpdb->prefix, 'wptests_');

		get_v1_data();
	}
}
