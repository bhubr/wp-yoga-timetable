<?php
$a = array (
    'professeurs_visibles' => array (
      'romain-hoogmoed' => 'romain-hoogmoed',
      'emma-thompson' => 'emma-thompson',
      'lucas-welch' => 'lucas-welch',
      'bertha-daniels' => 'bertha-daniels',
      'lynn-hicks' => 'lynn-hicks',
      'frances-arnold' => 'frances-arnold',
    ),
    'jours_visibles' => array (
      'lundi' => 'lundi',
      'mardi' => 'mardi',
      'mercredi' => 'mercredi',
      'jeudi' => 'jeudi',
      'vendredi' => 'vendredi',
      'samedi' => 'samedi',
    ),
  'heure_mini' => '540',
  'heure_maxi' => '1380',
  'granularite' => '15',
  'day_duration' => 840,
);
die("\n" . serialize($a) . "\n");