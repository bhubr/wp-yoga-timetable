<?php
/*
Plugin Name: WP Yoga Timetable
Plugin URI: https://github.com/bhubr/wp-yoga-timetable
Description: Gérer l'affichage d'un planning de cours de yoga
Version: 1.4.3 (August 30th, 2017)
Author: Benoit Hubert
Author URI: http://benoit.hubert2.free.fr
Copyright: 2011-2013 Benoit Hubert

GNU General Public License, Free Software Foundation <http://creativecommons.org/licenses/GPL/2.0/>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

require 'wpytt-config.php';
require 'wp-yoga-timetable-template.php';
require 'wp-plugin-form-helper.php';
require 'wpytt-data-functions.php';
require 'vendor/autoload.php';
require 'classes/wpytt-tarifs-handler.php';

// if(WP_DEBUG === true) {
//     $whoops = new \Whoops\Run;
//     $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
//     $whoops->register();
// }

 class WP_YogaTimeTable_Plugin {

    private $_config;
    
    private $_path;
    
    // Définitions de sous-pages qui seront insérées dans le menu d'administration
    private $_subpages = array( 
        'wpytt_options'      => "Options générales",
        'wpytt_options_css'  => "Apparence du tableau",
        'wpytt_import_export'  => "Import/export de données",
        // 'wpytt_responsive_test' => "Test de rendu adaptatif"
    );
    private $_metaBoxes = array(
        'wpytt_meta_box' => array(
            'title' => 'Paramètres du cours',
            'context' => 'normal',
            'type_for' => 'cours-yoga'
        ),
        'wpytt_person_meta_box' => array(
            'title' => 'Données supplémentaires',
            'context' => 'normal'
        )
    );
    private $_trDomain;

        /* Settings object */
    var $settings;
    private $_optionsUpdated = false;
    public $templater;
    public $tarifs;
    var $optSets = array();
    
    private $_optionForms = array();
    private $_postMetaForms = array();
    private $_userMetaForms = array();
    
    
    /**
     * Class constructor
     */
    function __construct() {
        // Get a Settings object instance
        $this->_path = plugin_dir_path( __FILE__ );
        add_action( 'activate_wp-yoga-timetable', [$this, 'setInitialSettings']);
        $options = $this->getAndSetInitialSettings();
        // add_action( 'init', array( $this, 'customTypesInit' ), 1 );
        $this->customTypesInit();
        $this->_metaBoxes['wpytt_person_meta_box']['type_for'] = $options['person_post_type'];
        // Ajouter les meta boxes dans la page d'edition d'un cours
        add_action( 'admin_init', array( $this, 'addMetaBoxes' ) );
        // Ajouter les sous-pages d'administration, définies par $_subpages
        add_action( 'admin_menu', array( $this, 'addAdminPages' ) );
        // Charger la CSS du tableau dans la partie utilisateur du site
        add_action( 'wp_print_styles', array( $this, 'loadTableCss' ) );
        add_action( 'admin_print_styles', array( $this, 'loadStyles' ) );
        add_action( 'wp_print_scripts', array( $this, 'loadScripts' ) );
        // Ceci est un essai: appeler wp-admin/admin-ajax.php?action=my_action
        // déclenche ce callback, voir plus loin
        add_action( 'wp_ajax_my_action', array( $this, 'my_action_callback' ) );
        foreach( array_keys( $this->_subpages ) as $slug ) {
            $this->settings[$slug] = get_option( $slug );
        }
        $this->templater = new WP_YogaTimeTable_Templater( $this );
        $this->tarifs_handler = new WP_YogaTimeTable_Tarifs_Handler();
        add_filter( 'pre_update_option_wpytt_options', array( $this, 'computeDayHeight' ), 10, 2 );
        add_action( 'update_option_wpytt_options_css', array( $this->templater, 'regenerateCss' ), 10, 2 );
        add_action( 'update_option_wpytt_options', array( $this, 'regenerateTable' ) );
        add_action( 'save_post', array( $this, 'submitMetaBox' ) );
        add_action( 'trashed_post', array( $this, 'regenerateTable' ) );
        add_filter( 'wp_insert_post_data', array( $this, 'changeMenuOrder' ), 99, 2);
        // add_shortcode( 'wpytt_responsive', array( $this, 'renderResponsive' ) );
        add_shortcode( 'wpytt_timetable', array( $this, 'render' ) );
        add_shortcode( 'wpytt_tarifs_table', array( $this->tarifs_handler, 'render' ) );
    }


    function getAndSetInitialSettings() {
        $defaults = array (
            'person_post_type' => 'post',
            'professeurs_visibles' => array (
            ),
            'jours_visibles' => array (
              'lundi' => 'lundi',
              'mardi' => 'mardi',
              'mercredi' => 'mercredi',
              'jeudi' => 'jeudi',
              'vendredi' => 'vendredi',
              'samedi' => 'samedi',
            ),
          'heure_mini' => '540',
          'heure_maxi' => '1380',
          'granularite' => '15',
          'day_duration' => 840,
        );
        $options = get_option('wpytt_options');
        if( ! $options ) {
            $options = $defaults;
            add_option('wpytt_options', $options);
        }
        return $options;
        // beware of the infinite loop... check GET args to prevent infinite redirection
        // if( ! array_key_exists('person_post_type', $options)) {
        //     wp_redirect('/wp-admin/edit.php?post_type=cours-yoga');
        //     exit;
        // }
    }

    // function renderResponsive() {
    //     $renderer = $this->templater->getResponsiveRenderer();
    //     return $renderer->render(false);
    // }
    
    function render() {
        // $renderer = $this->templater->getRenderer();
        return $this->templater->render(false);
    }
    
    /**
     * Recalcule la durée d'un jour, à partir des paramètres heure_maxi et heure_mini
     */
    function computeDayHeight( $new, $old ) {
        $new['day_duration'] = $new['heure_maxi'] - $new['heure_mini'];
        return $new + $old;
    }

    /**
     * Rend le tableau dans un document html, intégrable dans une iFrame
     */
    function my_action_callback() {
        global $wpdb; // this is how you get access to the database
        
        if( !current_user_can( 'manage_options' ) ) die();
        echo '<html><head><title>AJAX Request</title>';
        $css1 = plugins_url('timetable.css', __FILE__);
        echo '<link rel="stylesheet" href="' . $css1 . '" type="text/css" media="screen" />';
        echo '<meta http-equiv="Content-Type" content="text/HTML; charset=UTF-8" />';
        echo '</head><body>';
        echo $this->templater->getRenderer()->render( false );
        echo '</body></html>';
        
        die(); // this is required to return a proper result (trad: terminer proprement l'appel ajax)
    }

    /**
     * Recupere le path de base du plugin
     */
    function getPath() {
        return $this->_path;
    }

    /**
     * Enregistre le Custom Post Type pour les classes de yoga, et la taxonomie associée
     */
    function customTypesInit() {
        register_post_type( 
            'cours-yoga', array(
                'labels' => array(
                    'name'                  => 'Cours',
                    'singular_name'         => 'Cours',
                    'add_new'               => 'Ajouter cours',
                    'add_new_item'          => 'Ajouter cours',
                    'edit_item'             => 'Editer le cours',
                    'new_item'              => 'Nouveau cours',
                    'view_item'             => 'Voir cours',
                    'search_items'          => 'Chercher cours',
                    'not_found'             => 'Aucun cours trouv&eacute;',
                    'not_found_in_trash'    => 'Aucun cours dans la Corbeille;'
                ),
                // 'public' => true,
                'capability_type' => 'page',
                'supports' => array('title', 'page-attributes' ),
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!! remplacer stylesheet_dir par plugin_dir !!!!!!!!!!!!!!!!!!
                //    'menu_icon' => get_stylesheet_directory_uri() . '/images/slides.png',
                'menu_position' => 8,
                //'show_ui' => ( current_user_can( 'manage_options' ) ? true : false ),
                'show_ui' => true
                )
        );
        register_taxonomy(  
        'niveau-cours',
        'cours-yoga',
            array(  
                'hierarchical' => true,  
                'label' => 'Niveaux de cours',  
                'query_var' => true,  
                'rewrite' => true  
            )
        );
    }                           

    /** 
     * Ajouter meta box pour enrichir le Custom Post Type "cours-yoga" 
     */
    function addMetaBoxes() {
        foreach( $this->_metaBoxes as $boxSlug => $boxParams ) {
                add_meta_box(   $boxSlug, $boxParams['title'], array( $this, $boxSlug ), 
                                $boxParams['type_for'], $boxParams['context'] );
        }
    }


    /**
     * Contenu de la meta box pour cours de yoga
     */
    function wpytt_meta_box( $post ) {
        include( 'includes/wpytt_meta_box.php' );
    }
    
    /**
     * Contenu de la meta box pour cours de yoga
     */
    function wpytt_person_meta_box( $post ) {
        include( 'includes/wpytt_person_meta_box.php' );
    }
    
    function doMetaBox( $boxSlug ) {
        echo "<input type='hidden' name='meta_boxes[{$boxSlug}]' value='$boxSlug' />";
        
    }

    /** Register the admin subpages */
    function addAdminPages() {
        foreach( $this->_subpages as $slug => $title ) {
            add_submenu_page( 'edit.php?post_type=cours-yoga', $title, $title, 'manage_options', $slug, array( $this, $slug ) );
            register_setting( $slug, $slug, array( $this, 'validateSettings' ) );
        }
    }
        
    /**
     * Validate input : not used yet... Todo : check validity of color field, for manual input
     */
    function validateSettings( $input ) {
        return $input;
    }
    
    /**
     * WP attribue un champ menu_order aux pages, pour les ordonner, lorsqu'on n'utilise pas le menu configurable.
     * On détourne cet usage pour attribuer un ordre aux cours.
     * Le premier cours du lundi, par exemple, aura l'ordre le plus bas,
     * et le dernier cours du vendredi aura l'ordre le plus haut.
     * L'ordre est calculé à partir des heures de début et jour de la semaine.
     */
    function changeMenuOrder($data, $postarr)
    {   
            if( !isset( $postarr['wpytt_meta_box'] ) ) return $data;

            // If it is our form has not been submitted, so we dont want to do anything
            if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
            if( $data['post_type'] != 'cours-yoga' ) {
                return $data;
            }
            $days = array_keys( getData_jours_visibles() );
            $dayPos = array_search( $postarr['wpytt_meta_box']['jour_semaine'], $days ) + 1;
            // note : un jour = 1440 minutes
            $menuOrder = ($dayPos * 1440 ) + ( 0 + $postarr['wpytt_meta_box']['heure_debut'] );
            $data['menu_order'] = $menuOrder;
            return $data;
    }

    /**
     * Régénérer la table, après une mise à jour.
     */
    function regenerateTable() {
        $templater = $this->templater;

        // Ecrire en base de données (writeTable) la nouvelle version du tableau (render)
        $templater->dbIo()->writeTable( 'time_table', $templater->generateTable() );   
        
        // Supprimer les vieilles tables
        $this->removeOldTables();       
    }

    // Beware not to save post to DB here as it creates infinite loop !!!
    // OR deactivate the action...
    function submitMetaBox($postId) {
        if( isset( $_POST['wpytt_meta_box'] ) ) {
            // Ajouter les post meta au post, a partir des données entrées via la meta box
            update_post_meta( $postId, 'professeur', $_POST['wpytt_meta_box']['professeur'] );
            update_post_meta( $postId, 'jour_semaine', $_POST['wpytt_meta_box']['jour_semaine'] );
            update_post_meta( $postId, 'heure_debut', $_POST['wpytt_meta_box']['heure_debut'] );
            update_post_meta( $postId, 'heure_fin', $_POST['wpytt_meta_box']['heure_fin'] );
            update_post_meta( $postId, 'modele', $_POST['wpytt_meta_box']['modele'] );
            if( $_POST['wpytt_meta_box']['modele'] == 'custom' ) {
                update_post_meta( $postId, 'modele_custom', $_POST['wpytt_meta_box']['modele_custom'] );
            }
            $templater = $this->templater;
            remove_action( 'save_post', array( $this, 'submitMetaBox' ) );
            $templater->dbIo()->writeTable( 'time_table', $templater->generateTable() );
            $this->removeOldTables();
            add_action( 'save_post', array( $this, 'submitMetaBox' ) );
        }
        if( isset( $_POST['wpytt_person_meta_box'] ) ) {
            update_post_meta( $postId, 'first_name', $_POST['wpytt_person_meta_box']['first_name'] );
            update_post_meta( $postId, 'short_name', $_POST['wpytt_person_meta_box']['short_name'] );
        }
    }

    /** Remove old table versions, we dont wanna bloat the DB */
    function removeOldTables() { 
        global $wpdb;
        $olds = get_posts( 'post_type=time_table' );
        $IDs = array();
        foreach( $olds as $old ) {
            $IDs[] = $old->ID;
        }
        $minID = min( $IDs );
        $wpdb->query( "DELETE from $wpdb->posts WHERE post_type='time_table' AND ID < $minID" );
    }

    /**
     * Display a navigation menu below the page title, to give easier access to this plugin's subpages
     */
    function formPagesNavMenu( $selected ) {
        echo '<h3>';
        $print_sep = false;
        foreach( $this->_subpages as $slug => $title ) {
            $separator = $print_sep ? '&nbsp;|&nbsp;' : '';
            $item_class = ( $selected == $slug ) ?  " class='select'" : "";
            echo "$separator<span$item_class><a href='admin.php?page=$slug'>$title</a></span>";
            $print_sep = true;
        }
        echo '</h3>';
    }
    
    /**
     * Get form slug from its file name (e.g. calling with argument plugindir/forms/myform.php will return myform )
     */
    function getFormFromFile( $file ) {
        $form = explode( DIRECTORY_SEPARATOR, $file );
        $form = array_reverse( $form );
        $form = substr( $form[0], 0, -4 );
        return $form;
    }

    /**
     * Get page title from slug
     */
    function getPageTitle( $slug ) {
        $data = $this->_subpages[$slug];
        return $data['title'];
    }
    
    /**
     * Load per-page CSS files
     */
    function loadStyles() {
        global $pagenow;
        if( !current_user_can( 'manage_options' ) ) return;
        
        // if( in_array( $pagenow, array( 'post.php', 'post-new.php' ) ) )
        //     wp_enqueue_style('table_style', plugins_url('/timetables-gen.css', __FILE__) );
        else if( in_array( $pagenow, array( 'admin.php', 'edit.php' ) ) && isset( $_GET['page'] ) ) {
            switch ( $_GET['page'] ) {
                case 'wpytt_options_css' :
                    wp_enqueue_style('colorpicker_style_main', plugins_url('/colorpicker/css/colorpicker.css', __FILE__) );
                    wp_enqueue_style('colorpicker_style_layout', plugins_url('/colorpicker/css/layout.css', __FILE__) );
                    break;
                // Dropped... there used to be a prototype interfacing with Google Calendar
                // case 'wpytt_options_agenda' :
                //     wp_enqueue_style('datepicker_style', plugins_url('/css/ui-lightness/jquery-ui-1.8.16.custom.css', __FILE__) );
                //     wp_enqueue_style('gc_admin_style', plugins_url('/css/wpytt-gc-admin.css', __FILE__) );
                default :
                    break;
            }
        }
    }

    // CSS Tableau
    function loadTableCss() {
        $current_user = wp_get_current_user();
        if ( is_admin() ) return;
        wp_enqueue_style('table_style', plugins_url('/timetable.css', __FILE__) );
        //wp_enqueue_style('checkboxes_style', plugins_url('/cb-css/form.css', __FILE__) );
        //wp_enqueue_style('checkboxes_style', plugins_url('/pretty-checkboxes/css/prettyCheckboxes.css', __FILE__) );
    }

    /** 
     * Chargement conditionnel, par page des Javascripts
     */
    function loadScripts() {
        global $pagenow, $controls_js_file;
        // bundled in theme js
        // if( !is_admin() ) {
        //     $controls_js_url = plugins_url( 'js/wp-yoga-timetable-controls.js', __FILE__);
        //     wp_enqueue_script( 'wpytt_table_controls', $controls_js_url, array( 'jquery' ) );
        // }
        if( !current_user_can( 'manage_options' ) ) return;
        
        if ( in_array( $pagenow, array( 'post.php', 'post-new.php' ) ) ) {
            wp_enqueue_script('wtooltip', plugins_url('js/wtooltip.min.js', __FILE__) );
            wp_enqueue_script('js_utils', plugins_url('js/wp-yoga-timetable.js', __FILE__) );
        }
        else if( in_array( $pagenow, array( 'admin.php', 'edit.php' ) ) && isset( $_GET['page'] )  ) {
            switch ( $_GET['page'] ) {

                case 'wpytt_options_css' :
                    wp_enqueue_script('js_colorpicker_main', plugins_url('/colorpicker/js/colorpicker.js', __FILE__) );
                    wp_enqueue_script('js_colorpicker_eye', plugins_url('/colorpicker/js/eye.js', __FILE__) );
                    wp_enqueue_script('js_colorpicker_utils', plugins_url('/colorpicker/js/utils.js', __FILE__) );
                    wp_enqueue_script('js_colorpicker_layout', plugins_url('/colorpicker/js/layout.js', __FILE__) );
                    wp_enqueue_script('js_wpytt_colorpicker_client', plugins_url('/js/wp-yoga-timetable-coul-niv.js', __FILE__) );
                    break;


                default :
                    break;
            }
        }

    }

    /**
     * Magic function related to admin subpages
     * Each subpage is registered by its "slug"
     * The associated callback function's name is the slug
     * This includes a separate filename holding the callback stuff, with the slug as its basename
     * This avoids bloating the class with HTML stuff
     */
    function __call( $name, $arguments ) {
            if( !in_array( $name, array_keys( $this->_subpages ) ) )
                die( 'invalid __call' );
            if( !empty( $_POST ) && isset( $_POST[$name] ) ) {
            // echo "includes/{$name}_submit.php ";
            // echo file_exists( __DIR__ . "/includes/{$name}_submit.php") ?  'exists' :'nope' . "\n";
            // var_dump($_POST);die($name);
                if( file_exists( __DIR__ . "/includes/{$name}_submit.php" ) ) {
                    include( __DIR__ . "/includes/{$name}_submit.php" );
                }
                if( ! empty($_POST[$name]) ) {
                    update_option( $name, $_POST[$name] );
                    $this->settings[$name] = get_option( $name );
                    $this->_optionsUpdated = true;
                }
            }
            include( "includes/$name.php" );
    }
    
};



add_action( 'init', 'initPlugin', 1 );
function initPlugin() {
    global $wpytt;
    $wpytt = new WP_YogaTimeTable_Plugin();
    load_plugin_textdomain( 'wpytt', false, dirname( plugin_basename( __FILE__ ) ) . '/lang'  );
}



?>