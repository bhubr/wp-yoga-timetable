// Contrôles mooTools pour le tableau de planning
// la version jQuery existe (même fichiers sans suffixe -mooTools)
// mais elle devait avoir un bug ou n'être pas synvchro avec cette version


var  pmask_recompute = 0;
var  nmask_recompute = 0;
var recomputeEachTime = true;

window.addEvent('domready', function(){

	// c'est du mooTools mais relativement explicite si on connait jQuery
	// sauf que si mon souvenir est bon, ici $ est un sélecteur qui renvoie UN
	// élément, et $$ un sélecteur qui renvoie un ENSEMBLE d'éléments

	// Quand on clique sur une checkbox, on va vérifier son état et modifier l'état actuel du masque
	// - si c'est une checkbox prof, on modifie le masque prof: pmask
	// - si c'est une checkbox niveau, idem masque niveau: nmask

	$$('.cbprof').addEvent('click', function(event){

		// Il existe deux méthodes pour recalculer le masque courant...
		// On a choisi la 1ère, plus fiable, que ce soit pour les profs ou pour les niveaux
		if( recomputeEachTime)
			recomputeMaskAll( 'pmask', null );
		else
			recomputeMask( 'pmask',  this );

		tableUpdate( '.' + this.get( 'name' ), recomputeEachTime );
		
	});

	$$('.cbniveau').addEvent('click', function(event){

		if( recomputeEachTime)
			recomputeMaskAll( 'nmask', null );
		else
			recomputeMask( 'nmask',  this );
		tableUpdate( '.' + this.get( 'name' ), recomputeEachTime );

		});

});

// Mettre à jour la table
// selectedClass c'est le name de la checkbox, préfixé du . pour cibler la classe au sens css
// par exemple .n1, .n2, etc. pour cb niveau ou .yann-le-boucher, etc; pour cb prof
function tableUpdate( selectedClass, recomputeEachTime ) {

	var pmask = $( 'pmask' ).get( 'value' );
	var nmask = $( 'nmask' ).get( 'value' );

	// LOOP START
	// On cherche toutes les cases de cours avec la classe selectedClass
	// a chaque iteration thisDiv sera une case (càd une div)
	$$( selectedClass ).each(function(thisDiv){

		// on récupère les enfants de cette div/case
		var divChildren = thisDiv.getChildren();
		// le 1er enfant est le champ input dont la value contient:
		// le masque prof de la case (par ex. 1 pour Yann, 2 pour Luc)
		// et le masque niveau(X) (par ex. 8 + 16 pour cours N4,5)
		// les deux étant séparés par un pipe |
		// On récupère donc les deux valeurs en splittant la value sur le pipe
		var masksCaseField = $(divChildren[0]).get('value') ;
		var masksCase = masksCaseField.split( '|' );
		// On convertit en entier les deux composantes
		var prof = parseInt( masksCase[0] );
		var niveaux = parseInt( masksCase[1] );

		// showThis va être un flag (booléen) qui insique si on montre ou cache cette div
		var showThis;

		// C'est la 1ere méthode qui est utilisée (recomputeEachTime = true)
		if( recomputeEachTime ) {
			// Pour montrer la case, il faut que :
			// - la combinaison par ET logique de son masque prof et du masque prof global des checkboxes soit > 0
			// - idem pour la combinaison de son masque niveaux et du masque niveaux des checkboxes
			// Exemple: si seule checkbox N4 est cochée, le masque des niveaux (nmask_recompute) vaut 8.
			// Si le masque de cette case (niveaux) correspond, par exemple si cette case est un cours N4,5 son
			// niveau est 8 + 16, le ET logique avec le nmask_recompute vaut donc 8.
			// Et si cette case a pour prof Luc et que au moins la case de Luc est cochée, alors le ET logique pour
			// les masques profs est > 0 aussi, et alors on montre la case
			showThis = ( ( (prof & pmask_recompute) > 0 ) && ( (niveaux & nmask_recompute) > 0 ) );
		}
		else {
			showThis = ( ( (prof & pmask) > 0 ) && ( (niveaux & nmask) > 0 ) );
		}

		// Effectivement montrer si on le doit, cacher sinon
		if( showThis )
			thisDiv.removeClass( 'hiddenelem' );
		else
			thisDiv.addClass( 'hiddenelem' );
		});

}

// Cette méthode, recomputeMask,
// NE SEMBLE PAS TRES BIEN MARCHER !!
// Gardé "au cas où", mais préférer recomputeMaskAll() ci-dessous
function recomputeMask( maskField, cbObject ) {

		var isAdd = cbObject.get( 'checked' );
		var value = cbObject.get( 'value' );
		
		// Determiner le "poids" du "masque" global des checkbox de ce type
		var maskValue = parseInt( $( maskField ).get( 'value' ) );
		
		// Calculer la nouvelle valeur du masque (profs ou niveaux)
		var newMaskValue = 0;
		var thisValue = parseInt( value );
		if( isAdd )
			newMaskValue = maskValue + thisValue;
		else
			newMaskValue = maskValue - thisValue;
		
		$( maskField ).set( 'value', newMaskValue );
		//newMaskValue = jQuery('input[name="' + maskInput + '"]').val();
}

// Methode 1 : re-calculer les masques 
// On fait la somme de toutes les cb prof cochées (enfin, de leur masque, stocké dans leur value) dans pmask_recompute
// et idem, somme de toutes les cb niveau cochées (de leur masque, donc) dans nmask_recompute
function recomputeMaskAll( maskField, cbObject ) {

		pmask_recompute = 0;
		nmask_recompute = 0;
		
		$$( '.cbprof' ).each( function( cb ) {
			isChecked = ( cb.get('checked') == true );
			if( isChecked ) pmask_recompute += parseInt( cb.get( 'value' ) );
		});
		$$( '.cbniveau' ).each( function( cb ) {
			isChecked = ( cb.get('checked') == true );
			if( isChecked ) nmask_recompute += parseInt( cb.get( 'value' ) );
		});
}

