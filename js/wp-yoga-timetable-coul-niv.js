// Pour la page d'options CSS, utilise des colorPickers pour permettre de changer des couleurs

jQuery(document).ready(function($) {
	$('input.colorpickerField').each( function() { 
	    $(this).ColorPicker({
	            onSubmit: function(hsb, hex, rgb, el) {
	                    $(el).val(hex);
	                    $(el).ColorPickerHide();
	            },
	            onBeforeShow: function () {
	                    $(this).ColorPickerSetColor(this.value);
	            }
	    });
	});

	$('.colorpickerField').change( function() {

		// Determiner le nom du field change
		var theSelectorName = jQuery(this).attr('name');

		// Determiner la nouvelle valeur
		var selected = jQuery(this).val();
		
		// Trouver tous les fields ayant la meme valeur ( cad tous ceux de nom different)
		// et leur attribuer la valeur "none"
        jQuery("select."+selected).each(function () {
			if( jQuery(this).attr('name') != theSelectorName )
				jQuery(this).val( "none" );
				jQuery(this).removeClass( selected );
		});
 		jQuery(this).addClass( selected );
		
	});
});