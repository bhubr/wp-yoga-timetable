  // Version $ du contrôle de checkboxes
  // Si ça ne marche pas, ça ne doit pas en être loin...
(function($) {
  $(document).ready(function() {

    $('input[type=checkbox]').click( function() {

      /* Methode 1 : re-calculer les masques */
      var  pmask_recompute = 0;
      var  nmask_recompute = 0;
      var isChecked;
      $.each( $( '.cbprof' ), function() {
        isChecked = $(this).is(':checked');
        if( isChecked ) {
          pmask_recompute += parseInt( $(this).val() );
        }
      });
      $.each( $( '.cbniveau' ), function() {
        isChecked = $(this).is(':checked');
        if( isChecked ) {
          nmask_recompute += parseInt( $(this).val() );
        }
      });

      /* Methode 2 : utiliser les champs hidden pmask et nmask du tableau */
      // Determiner si la checkbox est active
      var checked = $(this).is(':checked');

      // Determiner s'il s'agit d'une cb prof ou niveau
      var maskInput = $(this).hasClass( 'cbprof' ) ? 'pmask' : 'nmask';
      // Determiner le "poids" du "masque" global des checkbox de ce type
      var maskValue = parseInt( $('input[name="' + maskInput + '"]').val() );

      // Calculer la nouvelle valeur du masque (profs ou niveaux)
      var newMaskValue = 0;
      var thisValue = parseInt( this.value );
      if( checked ) {
        newMaskValue = maskValue + thisValue;
      }
      else {
        newMaskValue = maskValue - thisValue;
      }

      $('input[name="' + maskInput + '"]').val( newMaskValue );
      newMaskValue = $('input[name="' + maskInput + '"]').val();

      var selectedClass = this.name;

      /* LOOP START */
      $.each( $( '.vyclass' ), function() {
        var $this = $(this);
        if( ! $this.hasClass( selectedClass ) ) {
          return;
        }

        var $maskInput = $this.find('input[type="hidden"]');

        var masksStr = $maskInput.attr("value");
        var masks = masksStr.split( '|' );
        var prof = parseInt(masks[0], 10);
        var niveaux = parseInt(masks[1], 10);
        var pmask = parseInt($('input[name="pmask"]').val(), 10);
        var nmask = parseInt($('input[name="nmask"]').val(), 10);

        // var showThis = ( ( (prof & pmask_recompute) > 0 ) && ( (niveaux & nmask_recompute) > 0 ) ); // Methode 1
        // var showThis = ( ( (prof & pmask) > 0 ) && ( (niveaux & nmask) === niveaux ) );
        var showThis = ( ( (prof & pmask) > 0 ) && ( (niveaux & nmask) > 0 ) );

        if( showThis ) {
          $(this).removeClass( 'hiddenelem' );
        }
        else {
          $(this).addClass( 'hiddenelem' );
        }

      });

    });

  /* Below : REMOVE in future versions */

  //  $('.vtt-box' ).addClass( 'dispnoneelem' );
      $('input[type=radio]').click( function() {

      // Get checkbox state : checked or not
      var checked = $(this).attr("checked");
      //alert( this.name + ' ' + this.id + ' ' + checked );
      var visibleElem = this;

      // If checked, show related elements, otherwise hide them
      $.each( $('input[name="' + this.name + '"]'), function() {
        //alert( visibleElem.id + ' ' + this.id );
        if( this == visibleElem ) {
          $('.' + this.id ).removeClass( 'dispnoneelem' );
          $('.' + this.id ).addClass( 'dispnormelem' );
        }
        else {
          $('.' + this.id ).removeClass( 'dispnormelem' );
          $('.' + this.id ).addClass( 'dispnoneelem' );
        }
        //  $('.' + this.id ).addClass( 'hiddenelem' );
      });
    });

  });
})(jQuery);