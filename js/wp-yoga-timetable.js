// Pas sûr que ce soit encore utilisé
"use strict";

var isEditionCours = false;
var slugJour, valJour, valHeure, ordre;
var toolTip = "Cette zone vous permet de pr&eacute;senter la case &agrave; votre<br>"
			+ "convenance, <span style='color: #722'><em>sous r&eacute;serve que la case soit assez grande</em></span>.<br>"
			+ "Les champs suivants seront remplac&eacute;s respectivement par&nbsp:<ul>"
			+ "<li><strong>$prof</strong> <em>Professeur associ&eacute; &agrave; ce cours</em></li>"
			+ "<li><strong>$niv</strong> <em>Niveau(x)</em></li>"
			+ "<li><strong>$jour</strong> <em>Jour</em></li>"
			+ "<li><strong>$debut</strong> <em>Heure de d&eacute;but</em></li>"
			+ "<li><strong>$fin</strong> <em>Heure de fin</em></li>"
			+ "</ul>";


			

jQuery(document).ready(function() {
	var select_prof = document.getElementById( 'select_prof' );
	isEditionCours = ( select_prof != undefined );	
	
	if( !isEditionCours ) return;

	//var str = jQuery( '#message' ).css('background', 'blue');  //.html().replace( 'l&rsquo;article', 'le tableau');
	var permalink = jQuery('#urlPostTableau').val();
	jQuery('#message').replaceWith( '<div id="message" class="updated"><p>Article mis &agrave; jour. <a href="' + permalink + '">Afficher le tableau</a></p></div>' );
	
	jQuery('input[name=menu_order]').attr( 'disabled', 'disabled' );
	
	jQuery('#publish').click( function(e) {
		jQuery('input[name=menu_order]').removeAttr( 'disabled' );
		var len = (jQuery('#title').val()).length;
		if( len == 0) {
			var jour = jQuery("#select_jour option:selected").text();
			var hdeb = jQuery("#select_heure_debut option:selected").text();
			var hfin = jQuery("#select_heure_fin option:selected").text();
			jQuery('#title').val(jour + ' ' + hdeb + '-' + hfin );
		}
	});
	
	jQuery('.tooltip').wTooltip({content: toolTip });
});



function recompute() {
	slugJour = jQuery( '#select_jour option:selected' ).val();
	valJour = jQuery( '#' + slugJour ).val();
	valHeure = jQuery( '#select_heure_debut option:selected' ).val();
	ordre = parseInt( valJour ) + parseInt( valHeure );
	jQuery('input[name=menu_order]').val( ordre );
}

