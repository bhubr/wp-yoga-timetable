<?php
// Bootstrap WP
include('../../../wp-load.php');

// Seulement pour les admins
if( !current_user_can( 'manage_options' ) ) { 
	header("HTTP/1.0 404 Not Found");
	die();
}
?>
<html><head><title>AJAX Request</title>

<?php $css1 = plugins_url('timetable.css', __FILE__); ?>
<link rel="stylesheet" href="<?php echo $css1 ; ?>" type="text/css" media="screen" />
<meta http-equiv="Content-Type" content="text/HTML; charset=UTF-8" />
</head><body>

<?php echo $wpytt->templater->getRenderer()->render(); ?>

</body></html>