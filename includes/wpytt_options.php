<?php
/*******************************************************
 * Formulaire d'options générales du plugin            *
 * Ce fichier est importé depuis l'admin, via          *
 * la fonction magique WP_YogaTimeTable_Plugin::__call *
 *******************************************************/
global $wpdb;
$slug = 'wpytt_options';
$options = $this->settings[$slug];

$pagetitle = $this->_subpages[$slug];
?>
<?php screen_icon(); ?>
<h2><?php echo esc_html( $pagetitle ); ?></h2>

<?php
$this->formPagesNavMenu( $slug );
if( $this->_optionsUpdated ) echo '<div id="update-nag">Options mises à jour.</div>';

// Profs
$person_post_type = $options['person_post_type'];
$sql_profs = "SELECT post_name,ID,post_title FROM {$wpdb->posts} WHERE post_type = '{$person_post_type}'";
$profs = $wpdb->get_results( $sql_profs, OBJECT_K );
?>
<div id="fsoptions" class="wrap" >
<form method="post" action="admin.php?page=wpytt_options">

<br><h3>Type de données professeur</h3>
<table>
    <?php
    echo WP_Plugin_FormHelper::generic_select(
            $slug, "person_post_type", "Type de données", $options['person_post_type'],
            get_post_types(), null, false, true );
    ?>
</table>

<br><h3>Professeurs visibles dans les s&eacute;lecteurs et les contr&ocirc;les du tableau</h3>
<table>
    <tr>
        <?php 
        $data_profs = array_key_exists('professeurs_visibles', $options) ?
            $options['professeurs_visibles'] : [];
        echo WP_Plugin_FormHelper::generic_checkbox( $slug, 'professeurs_visibles',
            'Professeurs visibles', $data_profs,
            $profs, 'post_title' );
        ?>
    </tr>
</table>

<h3>Jours visibles dans les sélecteurs et dans le tableau d'emploi du temps</h3>
<table>
    <?php
    echo WP_Plugin_FormHelper::generic_checkbox(
            $slug, "jours_visibles", "Jours visibles", $options['jours_visibles'],
            getData_jours_visibles(), null );

    ?>
</table> 
        
<br><h3>Heures minimale et maximale pour les s&eacute;lecteurs d&#39;heure</h3>
<table>
    <?php
    echo WP_Plugin_FormHelper::generic_select(
            $slug, "heure_mini", "Heure minimale", $options['heure_mini'],
            WP_Plugin_FormHelper::select_heures( 6, 10, 60 ), null, false, true );

    echo WP_Plugin_FormHelper::generic_select(
            $slug, "heure_maxi", "Heure maximale", $options['heure_maxi'],
            WP_Plugin_FormHelper::select_heures( 20, 23, 60  ), null, false, true );

    $granularites = array( '5' => '5 minutes', '10' => '10 minutes', '15' =>'15 minutes', '20' => '20 minutes', '30' => '30 minutes' );
    echo WP_Plugin_FormHelper::generic_select( 
            $slug, "granularite", "Granularit&eacute;", $options['granularite'],
            $granularites, null, false, true ); 
    ?>
</table>


<p class="submit">
<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
</p>
</form>
