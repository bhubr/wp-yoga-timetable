<?php
/**********************************************************
 * Boite de champs permettant de définir un cours de yoga *
 * Heure, jour, prof, etc.                                *
 **********************************************************/
$options = $this->settings['wpytt_options'];
$postID = $post->ID;

$slug = 'wpytt_person_meta_box';

$short_name = get_post_meta( $postID, 'short_name', true );

?>

<br/><input type='hidden' name='wpytt_person_meta_box[post_id]' value='<?php echo $postID; ?>' />

<h4>Méta-données</h4>

<p>Entrez les paramètres additionnels pour ce prof</p>

<table class="form-table">
    <tr>
        <td>Nom court<p>(exemple : prénom suivi des initiales du nom)</p></td><td><input name="wpytt_person_meta_box[short_name]" value="<?php echo $short_name; ?>" /></td>
    </tr>
</table>
