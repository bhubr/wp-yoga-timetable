<?php
/*******************************************************
 * Formulaire d'options d'apparence du tableau         *
 * Ce fichier est importé depuis l'admin, via          *
 * la fonction magique WP_YogaTimeTable_Plugin::__call *
 *******************************************************/
global $wpdb;
$slug = 'wpytt_options_css';
$options = collect($this->settings[$slug]);

$pagetitle = $this->_subpages[$slug];
?>
<div id="fsoptions" class="wrap" >
<?php screen_icon(); ?>
<h2><?php echo esc_html( $pagetitle ); ?></h2>

<?php
$this->formPagesNavMenu( $slug );
if( $this->_optionsUpdated ) echo '<div id="update-nag">Options mises à jour.</div>';
?>


<form method="post" action="admin.php?page=wpytt_options_css">

<br/>
<h3>Paramétrage de la feuille de style</h3>

<table>
<?php
        
    // $tailles = WP_Plugin_FormHelper::select_nombres( 80, 180, 10 );
    // echo WP_Plugin_FormHelper::generic_select( 
    //         $slug, "day_width", "Largeur jour", $options["day_width"],
    //         $tailles, null, false, true ); 

    $tailles = WP_Plugin_FormHelper::select_nombres( 15, 60, 5 );
    echo WP_Plugin_FormHelper::generic_select( 
            $slug, "hour_height", "Hauteur heure",  $options["hour_height"],
            $tailles, null, false, true ); 

    echo WP_Plugin_FormHelper::generic_select( 
            $slug, "sep_height", "Hauteur s&eacute;parateur",  $options["sep_height"],
            $tailles, null, false, true ); 

    $tailles_police = WP_Plugin_FormHelper::get_tailles_police();
    echo WP_Plugin_FormHelper::generic_select( 
            $slug, "font_size", "Taille de la police",  $options['font_size'],
            $tailles_police, null, false, true ); 
    echo '';
    
    // $tailles = WP_Plugin_FormHelper::select_nombres( 30, 60, 2 );
    // echo WP_Plugin_FormHelper::generic_select( 
    //         $slug, "head_width", 'Taille de la colonne "Heure"',  $options['head_width'],
    //         $tailles, null, false, true ); 
    
    $tailles = WP_Plugin_FormHelper::select_nombres( 1, 10, 1 );
    echo WP_Plugin_FormHelper::generic_select( 
            $slug, "cell_margin_top", 'Marge supérieure des cases (pixels)',  $options['cell_margin_top'],
            $tailles, null, false, true ); 
    ?>
</table>

<h3>Paramètres des cellules en sommet de colonne</h3>
<p>Ces paramètres permettent de spécifier l'apparence des en-têtes de colonnes contenant les jours.</p>

<table>

    <?php
    $tailles = WP_Plugin_FormHelper::select_nombres( 2, 30, 2 );
    echo WP_Plugin_FormHelper::generic_select( 
            $slug, "head_cell_padding_top", 'Espace entre texte et bordure supérieure',  $options['head_cell_padding_top'],
            $tailles, null, false, true ); 
    
    $tailles = WP_Plugin_FormHelper::select_nombres( 2, 30, 2 );
    echo WP_Plugin_FormHelper::generic_select( 
            $slug, "head_cell_border_bottom", 'Epaisseur de la bordure inférieure',  $options['head_cell_border_bottom'],
            $tailles, null, false, true ); 
    
    $tailles = WP_Plugin_FormHelper::select_nombres( 2, 30, 2 );
    echo WP_Plugin_FormHelper::generic_select( 
            $slug, "head_cell_height", 'Hauteur des cellules',  $options['head_cell_height'],
            $tailles, null, false, true ); 
    ?>
    <tr>
        <td>Couleur de fond</td>
        <td>
            <?php
            echo WP_Plugin_FormHelper::gen_color_picker( $slug, 'head_cell_color', null, $options['head_cell_color'] );
            ?>
        </td>
    </tr>
    <tr>
        <td>Couleur du texte des cases</td>
        <td>
            <?php
            echo WP_Plugin_FormHelper::gen_color_picker( $slug, 'cell_text_color', null, $options->get('cell_text_color') );
            ?>
        </td>
    </tr>
</table>
    
<h3>Couleurs associées aux niveaux de cours</h3>
<p>Cliquez sur les cases pour choisir la couleur d'affichage de chaque niveau de cours.</p>

<table>
    <?php
    $term_select_options = WP_Plugin_FormHelper::wpytt_get_niveaux_kva( '---' );
    
    foreach( $term_select_options as $term_slug => $term_name ) { ?>
    <tr>
        <td><?php echo $term_name; ?></td>
        <td>
            <?php echo WP_Plugin_FormHelper::gen_color_picker( $slug, 'term_color', $term_slug, $options['term_color'][$term_slug] ); ?>
        </td>
    </tr>    
    <?php
    } 
    ?>
</table>	

<h3>Aperçu</h3>
<?php $url = plugins_url( 'wp-yoga-timetable') . '/wp-yoga-timetable-ajax-frame.php'; ?>
<iframe id='preview' width='1100' height='500' frameborder='0' scrolling='auto' style='border: 1px solid #eee' src='<?php echo $url; ?>'></iframe>

    <p class="submit">
    <input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
    </p>
</form>

</div>

