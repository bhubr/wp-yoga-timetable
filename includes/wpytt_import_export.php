<?php
/*******************************************************
 * Import / Export de données                          *
 * Ce fichier est importé depuis l'admin, via          *
 * la fonction magique WP_YogaTimeTable_Plugin::__call *
 *******************************************************/
global $wpdb;
$slug = 'wpytt_import_export';
$options = $this->settings[$slug];

$pagetitle = $this->_subpages[$slug];
?>
<?php screen_icon(); ?>

<h2><?php echo esc_html( $pagetitle ); ?></h2>
<?php $this->formPagesNavMenu( $slug ); ?>
<div id="fsoptions" class="wrap" >

    <div style="width: 50%; display: inline-block;">

    	<h3>Import cours</h3>
    	<form method="post" action="admin.php?page=wpytt_import_export&action=import" enctype="multipart/form-data">
    		<input type="file" name="json" /><br>
    		<input type="submit" name="wpytt_import_export[do_import]" class="button-primary" value="<?php esc_attr_e('Import') ?>" />
    	</form>

    	<h3>Export cours</h3>
    	<form method="post" action="admin.php?page=wpytt_import_export&action=export">
    		<input type="submit" name="wpytt_import_export[do_export]" class="button-primary" value="<?php esc_attr_e('Export') ?>" />
    	</form>

    </div>
    <div style="width: 48%; display: inline-block;">

        <h3>Import tarifs</h3>
        <form method="post" action="admin.php?page=wpytt_import_export&action=import_tarif" enctype="multipart/form-data">
            <input type="file" name="json_tarif" /><br>
            <input type="submit" name="wpytt_import_export[do_import_tarif]" class="button-primary" value="<?php esc_attr_e('Import') ?>" />
        </form>

        <h3>Export tarifs</h3>
        <form method="post" action="admin.php?page=wpytt_import_export&action=export_tarif">
            <input type="submit" name="wpytt_import_export[do_export_tarif]" class="button-primary" value="<?php esc_attr_e('Export') ?>" />
        </form>

    </div>

</div>