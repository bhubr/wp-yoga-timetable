<?php
if( ! current_user_can('manage_options') ) {
	die();
}
if(! array_key_exists('action', $_GET)) {
	die();
}
if( array_search($_GET['action'], ['import', 'export', 'import_tarif', 'export_tarif']) === false ) {
	die();
}
$options = ['wpytt_options', 'wpytt_options_css'];
switch($_GET['action']) {
	case 'import':
		$json = file_get_contents($_FILES['json']['tmp_name']);
		$data = json_decode($json, true);
		foreach($data['options'] as $key => $value) {
			update_option($key, $value);
		}
		foreach($data['events'] as $event_data) {
			$post_id = wp_insert_post([
				'post_title' => $event_data['title'],
				'post_type'  => 'cours-yoga'
			]);
			unset($event_data['title']);
			foreach($event_data as $meta_key => $meta_value) {
				add_post_meta($post_id, $meta_key, $meta_value, true);	
			}
			
		}
		break;
	case 'export':
		$output = [
			'options' => [],
			'events'  => []
		];
		foreach($options as $option) {
			$output['options'][$option] = get_option($option, true);
		}
		// $person_post_type = $output['options']['wpytt_options']['person_post_type'];
		$events = get_posts([
			'post_type' => 'cours-yoga',
			'post_status' => 'publish',
			'numberposts' => -1
		]);
		$output['events'] = array_map(function($event) {
			$evt_arr = [
				'title' => $event->post_title
			];
			$evt_arr['starts_at']   = get_post_meta($event->ID, 'heure_debut');
			$evt_arr['ends_at']     = get_post_meta($event->ID, 'heure_fin');
			$evt_arr['person']      = get_post_meta($event->ID, 'professeur');
			$evt_arr['day_of_week'] = get_post_meta($event->ID, 'jour_semaine');
			return $evt_arr;
		}, $events);
		// header( 'Content-Type: application/json' );
		$json = json_encode( $output );
		// header( 'Content-Length: ' . strlen( $json ) );
		echo $json;
	case 'import_tarif':
		$json = file_get_contents($_FILES['json_tarif']['tmp_name']);
		$data = json_decode($json, true);
		foreach( $data as $niveau_slug => $niveau_data ) {
			$term = get_term_by( 'slug', $niveau_slug, 'niveau-cours' );
			foreach( $niveau_data as $meta_key => $meta_value ) {
				update_term_meta( $term->term_id, $meta_key, $meta_value );
			}
		}
		break;
	case 'export_tarif':
		$terms = get_terms( 'niveau-cours', array(
		    'hide_empty' => false,
		) );
		$output = [];
		foreach( $terms as $term ) {
			$niveau_tarif = get_term_meta( $term->term_id, 'niveau_tarif', true );
			$niveau_duree = get_term_meta( $term->term_id, 'niveau_duree', true );
			$tarif_legende = get_term_meta( $term->term_id, 'tarif_legende', true );
			$ordre_niveau = get_term_meta( $term->term_id, 'ordre_niveau', true );
			$output[$term->slug] = [
				'niveau_tarif' => $niveau_tarif,
				'niveau_duree' => $niveau_duree,
				'tarif_legende' => $tarif_legende,
				'ordre_niveau' => $ordre_niveau
			];
		}
		echo json_encode( $output );
		die();
}
exit;
?>