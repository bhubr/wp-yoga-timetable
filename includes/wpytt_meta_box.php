<?php
/**********************************************************
 * Boite de champs permettant de définir un cours de yoga *
 * Heure, jour, prof, etc.                                *
 **********************************************************/
$options = $this->settings['wpytt_options'];
$postID = $post->ID;

$slug = 'wpytt_meta_box';

$prof = get_post_meta( $postID, 'professeur', true );
$jour = get_post_meta( $postID, 'jour_semaine', true );
$heure_debut = get_post_meta( $postID, 'heure_debut', true );
$heure_fin = get_post_meta( $postID, 'heure_fin', true );
$modele_meta = get_post_meta( $postID, 'modele', true );
$modele = empty( $modele_meta ) ? 'standard' : $modele_meta;
$modele_custom = get_post_meta( $postID, 'modele_custom', true );
?>

<h4>Paramètres d'horaire</h4>

<p>Choisissez le jour, l'heure de début et l'heure de fin du cours</p>

<table class="form-table">
    
    <?php 
    global $wpyth;
    if( class_exists( 'Yogi_Theme' ) && isset( $wpyth ) && $wpyth->has_wpml ) {
        $profs = $wpyth->get_posts_array( 'les-professeurs',  'fr',  'post_name,post_title', 'publish', OBJECT_K );
    }
    else {
        $profs = getData_professeurs_visibles();
    }
    // var_dump($profs);
    echo WP_Plugin_FormHelper::generic_select(
        $slug, "professeur", "Professeur", $prof,
        $profs, 'post_title', false, true );

    $jours = getData_jours_visibles();
    $joursVisibles = array();
    foreach( $options['jours_visibles'] as $j ) {
        $joursVisibles[$j] = $jours[$j];
    }
    echo WP_Plugin_FormHelper::generic_select(
        $slug, "jour_semaine", "Jour", $jour,
        $joursVisibles, null, false, true );

    $heure_mini = $options['heure_mini'] / 60;
    $heure_maxi = $options['heure_maxi'] / 60;
    $gran = $options['granularite'];
    $heures = WP_Plugin_FormHelper::select_heures( $heure_mini, $heure_maxi, $gran  );
    echo WP_Plugin_FormHelper::generic_select(
        $slug, "heure_debut", "Heure de début", $heure_debut,
        $heures, null, false, true );
    echo WP_Plugin_FormHelper::generic_select(
        $slug, "heure_fin", "Heure de fin", $heure_fin,
        $heures, null, false, true );

    ?>
   
</table>
<h4>Paramètres de la case</h4>

<p>Choisissez le modèle utilisé pour afficher la case dans le tableau</p>
<table class="form-table">
    
    <?php
    $modeles = getData_modele();
    
    echo WP_Plugin_FormHelper::generic_radio($slug, "modele",
        $modele, $modeles );
    echo WP_Plugin_FormHelper::generic_textarea( $slug, "modele_custom", $modele_custom );

    ?>
</table>