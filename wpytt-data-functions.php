<?php
	/**************************************
	 * Fonctions de production de données *
	 **************************************/
	
	function getData_professeurs_visibles() {
		// Profs
		global $wpdb;
		$options = get_option('wpytt_options');
		$person_post_type = $options['person_post_type'];
		$sql_profs = "SELECT post_name,ID,post_title FROM {$wpdb->posts} WHERE post_type = '{$person_post_type}' AND post_status='publish'";
		$profs = $wpdb->get_results( $sql_profs, OBJECT_K );
		return $profs; //get_posts('post_type=les-professeurs&suppress_filters=0');
	}
/*	
	function getData_heure_mini() {
		return WP_Plugin_FormHelper::select_heures( 7, 10, 60 );
	}
	
	function getData_heure_maxi() {
		return WP_Plugin_FormHelper::select_heures( 20, 24, 60 );
	}
*/
	function getData_granularite() {
		return array( '5' => '5 minutes', '10' => '10 minutes', '15' =>'15 minutes', '20' => '20 minutes', '30' => '30 minutes' );
	}

	function getData_displayed_table() {
		global $wpdb;
		$sql = "SELECT ID,post_title FROM {$wpdb->posts} WHERE post_type = 'post' AND post_status = 'publish'";
		$posts = $wpdb->get_results( $sql, OBJECT_K );
		return $posts; //get_posts('post_type=les-professeurs&suppress_filters=0');
	}
	
	function getData_page_or_post() {
		global $wpdb, $wpytt;
		$sql = "SELECT ID,post_title FROM {$wpdb->posts} WHERE post_type = 'post' AND post_status = 'publish'";
		$posts = $wpdb->get_results( $sql, OBJECT_K );
		$select_post = WP_Plugin_FormHelper::generic_select( 'wpytt_options',
			"select_post",$wpytt->optSets['wpytt_options']->select_post, $posts, 'post_title', 
			false, true ); 
		
//		$sql = "SELECT ID,post_title FROM {$wpdb->posts} WHERE post_type = 'page' AND post_status = 'publish'";
		$sql = "SELECT ID,post_title FROM {$wpdb->posts} WHERE post_type = 'page'";
		$posts = $wpdb->get_results( $sql, OBJECT_K );

		$select_page = WP_Plugin_FormHelper::generic_select( 'wpytt_options',
			"select_page", $wpytt->optSets['wpytt_options']->select_page, $posts, 'post_title', 
			false, true ); 
		
		//$post_or_page = $wpytt_options["post_or_page"];
		return array(
			"post" => $select_post, 
			"page" => $select_page 
		);
	}

	function getData_professeur() {
		// Profs
		global $wpdb, $wpytt;
		
		$profs_visibles = $wpytt->optSets['wpytt_options']->professeurs_visibles;
		$profs_visibles_str = "( '" . implode( "', '", $profs_visibles ) . "' )";

		$sql_profs = "SELECT post_name,ID,post_title FROM {$wpdb->posts} WHERE post_type = 'les-professeurs' AND post_name IN $profs_visibles_str";
		$profs = $wpdb->get_results( $sql_profs, OBJECT_K );
		return $profs;
	}
	
	function getData_jour_semaine() {
		global $wpytt;
		$shownCodes = $wpytt->optSets['wpytt_options']->jours_visibles;
		$days = getData_jours_visibles();
		$shownDays = array();
		foreach( $shownCodes as $code )
			$shownDays[$code] = $days[$code];
		return $shownDays;
			
	}
	
	function getData_jours_visibles() {
		return array(	'lundi' => 'Lundi', 'mardi' => 'Mardi', 'mercredi' => 'Mercredi', 
						'jeudi' => 'Jeudi', 'vendredi' => 'Vendredi', 'samedi' => 'Samedi', 'dimanche' => 'Dimanche' );

	}
	
	function getData_heure_debut() {
		global $wpytt;
		return WP_Plugin_FormHelper::select_heures( $wpytt->optSets['wpytt_options']->heure_mini / 60, $wpytt->optSets['wpytt_options']->heure_maxi / 60, $wpytt->optSets['wpytt_options']->granularite );
	}

	function getData_heure_fin() {
		global $wpytt;
		return WP_Plugin_FormHelper::select_heures( $wpytt->optSets['wpytt_options']->heure_mini / 60, $wpytt->optSets['wpytt_options']->heure_maxi / 60, $wpytt->optSets['wpytt_options']->granularite );
	}
	
	function getData_modele() {
//		$modele = get_post_meta( $post_id, 'modele', true );
//		if( empty( $modele ) ) $modele = 'standard';
		return	array(
			"standard" => "Standard", 
			"multiple" => "Niveaux multiples: 1er créneau long (ex : N3-4)", 
			"court" => "Court (ex: N4 venant apr&egrave;s cours N3-4)", 
			"multiple_inv" => "Niveaux multiples: dernier créneau long (ex : N3-4)", 
			"court_inv" => "Court (ex: N4 venant avant cours N3-4)", 
			"custom" => "Particulier (aide <a class='tooltip' href='#'>ici</a>) :"
		); 
	}
/*	
	function getLabels_box_model() {
		return array( 'standard' => 'A', 'multiple' => 'B', 'court' => 'C', 'custom' => 'D' );
	}
	function getData_box_model() {
		global $wpytt;
		// var_dump($wpytt->optSets['wpytt_options_css']->box_models);
		return $wpytt->options['wpytt_options_css']->box_models;
	}

	function getData_day_width() {
		return WP_Plugin_FormHelper::select_nombres( 80, 180, 10 );
	}
	
	function getData_hour_height() {
		return WP_Plugin_FormHelper::select_nombres( 15, 60, 5 );
	}
	
	function getData_sep_height() {
		return WP_Plugin_FormHelper::select_nombres( 15, 50, 5 );
	}

	function getData_font_size() {
		return WP_Plugin_FormHelper::get_tailles_police();
	}
	
	function getData_head_width() {
		return WP_Plugin_FormHelper::select_nombres( 30, 60, 2 );
	}
	
	function getData_cell_margin_top() {
		return WP_Plugin_FormHelper::select_nombres( 1, 10, 1 );
	}
	
	function getData_head_cell_padding_top() {
		return WP_Plugin_FormHelper::select_nombres( 2, 30, 2 );
	}
	
	function getData_head_cell_border_bottom() {
		return WP_Plugin_FormHelper::select_nombres( 1, 10, 1 );
	}
	
	function getData_head_cell_height() {
		return WP_Plugin_FormHelper::select_nombres( 8, 40, 4 );
	}
*/
	function getLabels_term_color() {
		return WP_Plugin_FormHelper::wpytt_get_niveaux_kva();
	}

?>