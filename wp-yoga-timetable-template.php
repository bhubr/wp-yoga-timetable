<?php
require 'classes/wpytt-renderer.php';
require 'classes/wpytt-html-renderer.php';
require 'classes/wpytt-responsive-renderer.php';
require 'classes/wpytt-table-io.php';

	
/**
 * La classe "Templater" (je ne savais pas quel autre nom lui trouver !)
 * Trois rôles : 
 * 1-  "mâcher le travail" du "Renderer" (la classe qui produit le HTML proprement dite)
 *     A partir des données des différents cours, et des options (profs visibles, jours activés, etc.)
 *     on va traiter ces données pour les rendre exploitables par le Renderer.
 * 2-  Cette classe "Templater" fait aussi la régénération de la feuille de style du tableau,
 *     à partir d'un "template" de CSS (d'où le terme templater ?), lorsqu'on met à jour les options de style.
 * 3-  Enfin, c'est ici qu'on fait la substitution de la balise [timetable] dans un post par le tableau (filterPost)
 */
class WP_YogaTimeTable_Templater {

	protected $_wpytt;
	protected $_opts;
	protected $_css;
	protected $_renderer;
	protected $_responsiveRenderer;
	protected $_filePath;
	protected $_fileIo;
	protected $_dbIo;

	protected $_teachers;
	
	function __construct( $wpytt ) {
		// L'objet principal
		$this->_wpytt = $wpytt;
		// Les options générales
		$this->_opts = $wpytt->settings['wpytt_options'];
		// Les options d'apparence
		$this->_css = $wpytt->settings['wpytt_options_css'];

		// Doit être appelé AVANT d'instancier les renderers
		$this->_teachers = $this->_queryTeachers();

		// Le renderer
		$this->_renderer = new WP_YogaTimeTable_HtmlRenderer( $this, $wpytt, $this->_opts, $this->_css );
		$this->_responsiveRenderer = new WP_YogaTimeTable_ResponsiveRenderer( $this, $wpytt, $this->_opts, $this->_css );
		// Le path du plugin
		$this->_filePath = $this->_wpytt->getPath();

		// Pas gênant d'instancier FileTableIO, on ne fait d'accès disque que sur appel à writeTable()
		$this->_fileIo = new WPYTT_FileTableIO( $this->_filePath, 'timetable.html.part' );
		$this->_dbIo = new WPYTT_DBaseTableIO();

	}

	/**
	 * Récupérer une référence au Renderer
	 */
	function getRenderer() {
		return $this->_renderer;
	}

	function getResponsiveRenderer() {
		return $this->_responsiveRenderer;
	}

	function generateTable() {
		return '<div class="wpytt-table-lg">' .
			$this->_renderer->render(false) .
			'</div><div class="wpytt-table-sm">' .
			$this->_responsiveRenderer->render(false) .
			'</div>';
	}

	function render() {
		return $this->_dbIo->readTable( 'time_table' );
	}


	/**
	 * Récupérer une référence à l'objet qui gère les Entrées/Sorties en base de données 
	 */
	function dbIo() {
		return $this->_dbIo;
	}
	
	/**
	 * Régénérer la feuille de style du tableau
	 * Les arguments $old et $new contiennent respectivement
	 * l'ancien et le nouveau contenu du tableau d'options CSS,
	 * cette fonction est appelée lors d'un update_option sur l'option wpytt_options_css
	 */
	function regenerateCss( $old, $new ) {

		// On récupère le nouveau tableau d'options
		$cssVars = $new;

		// Voir le code de computeGaps() ci-après : important à comprendre
		list( $intervals, $numBreakHours, $mask, $bitsPerHour ) = $this->computeGaps();

		// On va compléter le tableau et y ajouter des données, à partir :
		//  - des options juste validées 
		//  - de ce qu'on vient d'obtenir par l'appel à computeGaps()

		// Ici on calcule la hauteur en pixels d'un jour:
		// Durée du jour en minutes, divisée par 60, multipliée par hauteur en pixels d'une heure
		// On en retranche le nombre d'heures de pause dans une journée, multiplié par la hauteur d'une heure
		// On y rajoute le nombre d'intervalles de pause multiplié par la hauteur d'un "séparateur"
		// Enfin on y ajoute la hauteur de l'entête d'une colonne (case du label du jour + marges, padding, bordures)
		$cssVars['day_height'] = ( $this->_opts['day_duration'] * $cssVars['hour_height'] ) / 60
			- ( $numBreakHours * $cssVars['hour_height'] ) 
			+ count( $intervals ) * $cssVars['sep_height']
			+ $cssVars['head_cell_height'] + $cssVars['head_cell_padding_top'] + $cssVars['head_cell_border_bottom'];
		
		$numDays = count( $this->_opts['jours_visibles'] );
		$percentPerDay = 92.0 / $numDays;
		$cssVars['day_width'] = number_format($percentPerDay, 2);

		// On récupère les couleurs associées à chaque niveau
		$couleursNiveaux = $cssVars['term_color'];
		// On récupère les niveaux eux-mêmes.
		// L'ordre est important car un cours possédant plusieurs niveaux (n3, n4, n5) doit prendre la couleur
		// du niveau le plus bas (n3), ce qui est obtenu en mettant les définitions des classes css dans un certain ordre
		$terms_niveaux = WP_Plugin_FormHelper::wpytt_get_niveaux( 'DESC', 'slug' );
		$css = "/* Colors */\n";
		// On itère sur chaque niveau
		foreach( $terms_niveaux as $term_niveau ) {
			// Le "slug" c'est un identifiant court attaché au niveau, ex: "n1" pour "Niveau 1"
			$niveau = $term_niveau->slug;
			// On récupère la couleur pour ce niveau précis
			$couleur = $couleursNiveaux[$niveau];
			// On écrit dans la CSS la classe CSS correspondante à ce niveau
			$css .= ".$niveau , ._$niveau {"
				 . "\tbackground: #$couleur;} ";
		}

		// Ouvrir le template CSS et remplir un tableau avec ses lignes
		// On aurait pu faire un appel à file() ? Je viens d'essayer, marche pas :(
		$fh = fopen( $this->_filePath . 'timetable.template.css', 'r' );
		while( $line = fgets( $fh ) ) $css .= $line;
		fclose( $fh );

		// On va substituer les "variables" du template CSS à leurs vraies valeurs
		// (rem: on aurait pu utiliser un moteur comme SASS ou LESS... mais je connaissais pas)
		// Pour la plupart des variables, on prend directement les valeurs saisies dans le formulaire
		// d'options d'apparence du tableau. Pour certaines autres (hour_height, sep_height, font_size)
		// des ajustements sont nécessaires

		// On reconstruit deux tableaux:
		// Un avec les "tags" (style [tag]) à rechercher et remplacer
		// Un tableau de variables "ajustées" par lesquelles on remplace les tags
		$searches = array();
		$values   = array();
		foreach( $cssVars as $var => $value ) {
			// Decrement hours heights
			if( $var == 'hour_height' || $var == 'sep_height' ) $value -= 1;

			// Traduire choix de taille de fonte en pixels
			if( $var == 'font_size' ) {
				$tailles = WP_Plugin_FormHelper::get_tailles_police();
				$value = $tailles[$value];
			}

			if( $var == 'term_color' ) {
				continue;
			}
			$searches[] = "[$var]";
			$values[]   = $value;
		}
		// Substitution des variables (tags) par leur valeur,
		// str_replace remplace chaque item de $searches par
		// l'item correspondant de $values
		$css = str_replace( $searches, $values, $css );

		// Ouverture du fichier de CSS pour écriture
		$success = file_put_contents( $this->_filePath . 'timetable.css', $css );
		if( $success == false ) {
			$msg = 'Unable to open file: timetable.css';
			error_log( $msg );
			throw new Exception( $msg );
		}

		// On n'a pas besoin d'écrire la table sur disque finalement
		//	$this->_fileIo->writeTable( $this->_renderer->render( false ) );

		// Et enfin, on doit écrire une nouvelle version du tableau,
		// avec les options CSS mises à jour.
		// Le Renderer doit donc connaître les nouvelles options.
		$this->_renderer->setCss( $cssVars );
		$this->_dbIo->writeTable( 'time_table', $this->generateTable() );
	}
	

	/**
	 * Filtrer un post, en remplaçant [timetable] par le contenu du tableau
	 */
	function filterPost( $content ) {
		// Preg match setup : préparer le regex match
		$matches = array();
		$ids = array();
		$table_id = 0; // Also set $table_post in future versions ?
		$pattern_to_replace = array();
		// Case 1 : LEFT FOR FUTURE USE : we may have several tables in the future, managed by custom posts
		// That would be posts with type=time_table (see dbIo to see how we save time table to DB)
		$content = apply_filters('the_content', $content);
		if( preg_match( '/\[timetable id=(\d)+\]/', $content, $matches ) == 1 ) {
			// An ID was specified, then let's retrieve it
			preg_match( '/(\d)+/', $matches[0], $ids );
			$table_id = $ids[0];
			// Then get Custom post ---- WARNING !! This won't be a standard WP post but a CUSTOM post
			$table_post = get_post( $table_id );

			$pattern_to_replace[] = '/\[timetable id=(\d)+\]/';
		}
		// Co-dessous : le seul cas vraiment rencontré ici !
		// Case 2 : we only have ONE timetable containing all the (yoga) classes
		else if( preg_match( '/\[timetable]/', $content, $matches ) == 1 ) {
			$pattern_to_replace[] = '/\[timetable]/';
		}
		// Derniere possibilité: non trouvé: alors on retourne le contenu non filtré
		else {
			return $content;
		}
		
		// Chaîne à substituer à la balise [timetable] = contenu du dernier tableau sauvé
		$replacement_string = $this->_dbIo->readTable( 'time_table' );
		if( empty( $replacement_string ) ) {
			$replacement_string = $this->_fileIo->readTable( 'time_table' );
		}
		if( empty( $replacement_string ) ) {
							$replacement_string = "1";
				}
		// Avant on faisait le render ici : coûteux car régénération de tout le HTML à chaque visite
		// de la page Planning. Ceci dit un appel DB est coûteux aussi... encore qu'il est peut-être caché.
		// $replacement_string = $this->_renderer->render();

		// Substitution effective et retour
		$replacement_a = array( $replacement_string ); 
		return preg_replace($pattern_to_replace, $replacement_a, $content);
 
	}

	/**
	 * Renvoyer un "modèle de case"...
	 * Pour chaque cours, une case est affichée dans le tableau d'emploi du temps.
	 * Toutes ne contiennent pas le même texte: un cours "standard" (un niveau seulement)
	 * contiendra un certain texte.
	 * Outre le "standard", il y a les modèles "multiple", "court", "custom" (paramétrable 
	 * dans la meta box de la page d'édition du cours)
	 */
	function getBoxModel( $model ) {
		$models = array(
			// link open, levels string, start hour pretty, end hour pretty, link close, teacher
			"standard" => array(
				"string" => "{linkOpen}<strong>%s</strong>&nbsp;%s &mdash; %s{linkClose}<br>%s",
				"args"	 => array( 'levelsString', 'stHourPr', 'endHourPr', 'teacherNm' )
			),
			// link open, levels string, start hour pretty, end hour pretty, link close, teacher
			"multiple" => array(
				"string" => "{linkOpen}%s : d&eacute;but %s{linkClose}<br/>%s<br/>%s&nbsp;: fin %s",
				"args"	 => array( 'levelsString', 'stHourPr', 'teacherNm', 'firstLevelString', 'endHourPr' )
			),
			"court" => array(
				"string" => "{linkOpen}%s : fin %s{linkClose}",
				"args"	 => array( 'firstLevelString', 'endHourPr' )
			),
			// link open, levels string, start hour pretty, end hour pretty, link close, teacher
			"multiple_inv" => array(
				"string" => "{linkOpen}%s : d&eacute;but %s{linkClose}<br/>%s<br/>%s&nbsp;: fin %s",
				"args"	 => array( 'firstLevelString', 'stHourPr', 'teacherNm', 'levelsString', 'endHourPr' )
			),
			"court_inv" => array(
				"string" => "{linkOpen}%s : début %s{linkClose}",
				"args"	 => array( 'firstLevelString', 'stHourPr' )
			),
			"custom" => array(
				"string" => "{linkOpen}%s{linkClose}",
				"args"	 => array( 'modelCust' )
			)
		);
		// On renvoie le modèle demandé, correspondant à la post meta "modele" de ce cours
		return $models[$model];
	}
	
	/**
	 * Récupérer les jours activés (cochés dans la page d'options)
	 * Todo: code redondant avec wpytt-data-functions.php, rassembler les deux ?
	 */
	function getDays() {
		$shownCodes = $this->_opts['jours_visibles'];
		$days = array(	'lundi' => 'Lundi', 'mardi' => 'Mardi', 'mercredi' => 'Mercredi', 
						'jeudi' => 'Jeudi', 'vendredi' => 'Vendredi', 'samedi' => 'Samedi', 'dimanche' => 'Dimanche' );
		$shownDays = array();
		// Renvoie tableau associatif code => Label
		foreach( $shownCodes as $code ) {
			$shownDays[$code] = $days[$code];
		}
		return $shownDays;
	}

	/**
	 * Récupérer les professeurs,
	 * qui sont des Custom posts de type "les-professeurs".
	 * Pas de limite sur le nombre, statut publié, exclure n°361 (Les assistants)
	 * TODO : Mettre l'exclude non pas en dur, mais à partir d'une option ?
	 */
	function _queryTeachers() {
		global $wpdb;
		$results = $wpdb->get_results( "SELECT `element_id` FROM {$wpdb->prefix}icl_translations  WHERE `element_type` = 'post_les-professeurs' AND `language_code` = 'fr'", ARRAY_A );
		$mapped = array_map(function($item) {
			return $item['element_id'];
		}, $results);
		// var_dump($mapped);

		$args = array( 
			'post_type'        => $this->_opts['person_post_type'],
			'numberposts'      => 0,
			'post_status'      => 'publish', 
			'exclude'          => '361',
			'orderby'          => 'menu_order',
			'order'            => 'ASC',
			'post__in'         => $mapped
		);
		$wp_query = new WP_Query($args);
		if ( ! $wp_query->have_posts() ) {
			return array();
		}
		// var_dump('fire query');
		// $teacherObjects = get_posts(  );

		// l'option professeurs_visibles est un tableau des codes (slugs) des profs visibles
		$shownTeachers = $this->_opts['professeurs_visibles'];
		$teachers = array();
		// var_dump($wp_query);
		// Pour chaque prof rertrouvé en db, s'il fait partie des profs visibles, l'ajouter au tableau teachers
		// foreach( $teacherObjects as $teacherObj ) {
		while ( $wp_query->have_posts() ) {
			global $post;
			$wp_query->the_post();
			// var_dump(get_the_ID() . ' ' . get_the_title());
			// var_dump($post);
			$slug = $post->post_name;
			if( !in_array( $slug, $shownTeachers ) ) continue;
			// On remplace les espaces par des insécables dans le nom complet du prof
			// (ceci pour ne pas risquer d'avoir le prénom et nom éclatés sur 2 lignes dans les contrôles du tableau)
			$teacherShortName = get_post_meta($post->ID, 'short_name', true);
			$teacherName = str_replace( ' ', '&nbsp;', $post->post_title );
			// var_dump($teacherShortName . ' ' . $teacherName);
			if( empty( $teacherShortName ) ) {
				$teacherShortName = $teacherName;
			}
			else {
				$teacherShortName = str_replace( ' ', '&nbsp;', $teacherShortName );
			}
			$teachers[$slug] = $teacherShortName;
		}
		return $teachers;
	}

	function getTeachers() {
		return $this->_teachers;
	}
			
	/**
	 * Récupérer les cours... On va les trier aussi
	 */
	function getClasses() {
		global $wpdb;

		// D'abord récupérer les professeurs activés, visibles
		$teachers = $this->getTeachers();

		// On va construire un tableau de cours regroupés par jour de la semaine
		// 'lundi' => array( cours1, cours2), 'mardi' => array( cours3, cours4), ...
		$classesByDay = array();
		foreach( $this->getDays() as $code => $name ) {
			$classesByDay[$code] = array();
		}
			
		// On interroge les cours (Custom Posts de type "cours-yoga")
		//$lassesQuery = new WP_Query( "post_type=cours-yoga&post_status=publish&orderby=menu_order&order=ASC&meta_value=$slug_jour" );
		$sqlClasses = "SELECT ID,post_name,post_title FROM {$wpdb->posts} WHERE post_type = 'cours-yoga' AND post_status='publish' ORDER BY menu_order ASC";
		
		// On les récupère comme tableau, avec les ID des cours comme clés 
		$classResults = $wpdb->get_results( $sqlClasses, OBJECT_K );
		
		// Pour caque cours (ID cours => contenu du post #ID)
		// On va récupérer ses données ET méta-données (post_meta) et ajouter un item à classesByDay
		foreach( $classResults as $classId => $class ) {
			
			// un cours = titre, prof, jour, h. deb, h. fin

			// Identifiant du prof
			$teacherSlug = get_post_meta( $classId, 'professeur', true );
			// Nom en toutes lettres du prof
			$teacherName = $teachers[$teacherSlug];
			// Code jour, ex. 'lundi'
			$daySlug = get_post_meta( $classId, 'jour_semaine', true );
			
			// Heures de début et fin en minutes (valeurs brutes des post_meta)
			// et reformatées (09h00), pour l'affichage
			$startHour = get_post_meta( $classId, 'heure_debut', true );
			$startHourPretty = WP_Plugin_FormHelper::pretty_print_heure( $startHour );
			$endHour = get_post_meta( $classId, 'heure_fin', true );
			$endHourPretty = WP_Plugin_FormHelper::pretty_print_heure( $endHour );
			
			// Modèle de case associé au cours, et le cas échéant le modèle custom
			$model = get_post_meta( $classId, 'modele', true );
			$modelCustom = $model == 'custom' ? get_post_meta( $classId, 'modele_custom', true ) : null;
			
			// Structure contenant toutes les données du cours
			$classesByDay[$daySlug][$class->post_name] = array( 
				'id'		    => $class->ID, 
				'title'		  => $class->post_title, 
				'teacherSl'	=> $teacherSlug,
				'teacherNm'	=> $teacherName,
				'stHour'	  => $startHour,
				'endHour'	  => $endHour,
				'stHourPr'	=> $startHourPretty,
				'endHourPr'	=> $endHourPretty,
				'model'		  => $model,
				'modelCust' => $modelCustom
			);
		}
		// Sera récupéré par le renderer
		return $classesByDay;
	}

	/**
	 * Récupère des "bitmasks" pour les niveaux de cours (ATTENTION rien à voir avec les bitmasks
	 * de la méthode computeGaps plus bas)
	 * voir méthode getTermNiveaux() plus bas
	 */
	public function getClassLevelsBitmask() {
		$bitmaskPosition = 0;
		$classLevelsBitmask = 0;
		// Récupérer les niveaux de cours. KVA signifie simplement Key-Value Array
		// Ce sont donc les niveaux (catégorie spécialisée pour les Custom posts cours-yoga),
		// en tableau associatif identifiant => Label
		$levels = WP_Plugin_FormHelper::wpytt_get_niveaux_kva('');
		$masksPerLevel = array();
		// A chaque niveau de cours, on attribue une puissance de 2.
		// En partant de 2^0, puis 2^1, ... 2^(n-1) ou n = nombre de niveaux de cours
		// Pourquoi faire ? Exemple :
		// 'n1'     => 'Niveau 1'           --> 1,
		// 'n2'     => 'Niveau 2'           --> 2,
		// 'n3'     => 'Niveau 3'           --> 4, ...
		// 'medit'  => 'Atelier méditation' --> 32
		// Cela se traduit dans l'entête du tableau de planning par des cases à cocher
		// Si tout est coché, le bitmask a la valeur maximale, ici 63.
		// Si on décoche alors le n3, le bit 2 est à zéro, le masque vaut 59.
		// Les valeurs 1, 2, 4, ..., 32 sont stockées comme valeurs des checkboxes
		foreach( $levels as $slug => $name ) {
			// Décalage permettant de faire 2^x, ici x = bitmaskPosition
			$thisPositionMask = 1 << $bitmaskPosition;
			// Associer la valeur à l'identifiant
			$masksPerLevel[$slug] = $thisPositionMask;
			// Ajouter cette valeur au masque global
			$classLevelsBitmask += $thisPositionMask;
			// Incrémenter position
			$bitmaskPosition++;
		}
		// Masque global (toutes cases cochées)
		$masksPerLevel['__ALL__'] = $classLevelsBitmask;
		// On renvoie le tableau associant 'n1' => 1, ..., 'medit' => 32 (par ex.)
		return $masksPerLevel;
	}

	/**
	 * Ressemble à la méthode précédente dans le principe
	 * Simplement, cette fois ci non spécifique aux niveaux :
	 * marche à la fois pour les NIVEAUX et pour les PROFS
	 * Cette fois, au lieu de renvoyer juste un tableau slug => 2^x,
	 * on renvoie aussi le code html des checkboxes
	 * D'ailleurs c'est mal, ça devrait être laissé au renderer HTML !!
	 * $cbType peut valoir 'cbprof' ou 'cbniveau'
	 */
	public function getGenericBitmaskAndControls( $data, $cbType ) {
		$controls = array();
		$bitmaskPosition = 0;
		$bitmask = 0;
		$masks = array();
		foreach( $data as $slug => $name ) {
			$thisPositionMask = 1 << $bitmaskPosition;
			//echo "Pos $bitMaskPosition, mask=$thisPositionMask<br>";
			$masks[$slug] = $thisPositionMask;
			$bitmask += $thisPositionMask;
			$bitmaskPosition++;
			// Dans ce code html, l'attribut class = _$slug du <span> ouvrant sert à colorier la checkbox et le texte associé
			// en fonction du niveau. Ensuite la checkbox a un id (le slug du cours ou du prof) et une valeur de puissance de 2,
			// qui sera utilisée dans le code Javascript
			$controls[] = "  <span class='_$slug tabcontniv'><span class='tt-checkbox'><input class='$cbType' type='checkbox' id='$slug' name='$slug' value='$thisPositionMask' checked /></span>$name</span>\n";
		}
		$masks['__ALL__'] = $bitmask;
		return array( $masks, $controls );
	}
	
	/**
	 * Récupérer le(s) niveau(x) de cours associé(s) à un cours
	 * Attention cela renvoie une structure de donnée assez complexe,
	 * pas un simple tableau associatif.
	 * La finalité c'est notamment de pouvoir remplir les cases
	 * des cours à niveaux multiples, dans le renderer.
	 * En effet, si un cours est N3,N4,N5 on voudrait économiser de la place
	 * dans la case en n'écrivant que N3,4,5.
	 */
	public function getTermNiveaux( $classPostId ) {

		// Récupérer les niveaux associés au cours
		$terms_niveaux = get_the_terms( $classPostId, 'niveau-cours' );
		// Si aucun niveau associé à ce cours (aucune catégorie cochée), retourne false
		if( empty( $terms_niveaux ) ) return false;

		// A parir de là on va créer une structure de données.

		// Tableau pour les "slugs" (codes courts, 'n1', 'n2' etc.) des niveaux
		$slugs_niveaux = array();
		$niveaux = array();
		$niv_matches;
		$nivs_mask = 0;
		// ici on récupère les bitmasks des niveaux
		$nivs_masks = $this->getClassLevelsBitmask();
		foreach( $terms_niveaux as $term ) {
			// Ajouter le slug  à un tableau
			$slugs_niveaux[] = $term->slug;
			// Déterminer si le niveau est numéroté (N1, N2, N3 ... = OUI, Méditation = Non)
			$isNumbered = preg_match( '/(\d)+/', $term->name, $niv_matches ) > 0;
			// Suivant que c'est numéroté ou non, ajouter à niveaux[] juste le numéro, ou le nom complet
			// Ainsi 1, 2, 3... pour N1, N2, N3 dans le 1er cas, ou Atelier de Méditation dans le 2nd
			$niveaux[] = $isNumbered ? $niv_matches[0] : $term->name;
			// Par exemple si ce cours est à la fois de niveau 3, 4, 5, on va ajouter 2^3 + 2^4 + 2^5
			$nivs_mask += $nivs_masks[$term->slug];
		}
		// Rassembler les niveaux en les séparant juste par des espace : ce seront des classes css
		// Ex: "n3 n4 n5"
		$slugs_str = implode( ' ', $slugs_niveaux );
		// On obtiendra, par ex: "N3,4,5"
		$niveaux_str = ( $isNumbered ? 'N' : '' ) . implode( ',', $niveaux );
		$premier_niv_str = ( $isNumbered ? 'N' : '' ) . $niveaux[0];
		// Pour résumer, renvoie:
		// $niveaux: qq chose comme [3,4,5]
		// $niveaux_str: "N3,4,5"
		// $premier_niv_str: "N3"
		// $slugs_str: "n3 n4n n5"
		// $nivs_mask (tjrs pour cours N3,4,5): 4 + 8 + 16 = 28
		return array( $niveaux, $niveaux_str, $premier_niv_str, $slugs_str, $nivs_mask );
	}
	
	/**
	 * Calcul des intervalles d'heures vides.
	 * Par exemple s'il n'y a pas du tout de cours entre 13h45 et 17h15,
	 * on va regrouper les 3h entre 14h et 17h dans une seule petite case,
	 * ceci afin d'économiser de l'espace vertical
	 */
	function computeGaps() {
		
		// Récupérer paramètre de "granularité", càd l'unité minimale de temps pour régler
		// les heures mini et maxi.
		// Si granularite = 5 minutes on pourra choisir 9h00, 9h05, 9h10, etc.
		// Si granularite = 15 minutes on pourra choisir 9h00, 9h15, 9h30, etc.
		$granularity = $this->_opts['granularite'];
		// Si par malheur le paramètre a été resetté, on le met à 1 (pas de division par zéro !)
		if( $granularity == 0 ) $granularity = 1;
		
		// Calcul des limites de l'intervalle dans lequel on va chercher des intervalles vides.
		// On récupère les bornes en minutes.
		// Par ex. si l'heure minimale est 7h $heureMini vaut 420.
		// Si l'heure maximale est 22h $heureMaxi vaut 1320.
		$heureMini = isset( $this->_opts['heure_mini'] ) ? $this->_opts['heure_mini'] : 0;
		$heureMaxi = isset( $this->_opts['heure_maxi'] ) ? $this->_opts['heure_maxi'] : 1440;

		// On calcule les trous dans l'emploi du temps comme un "bitmask", c'est-à-dire
		// qu'un "bit" = une unité de temps (qui vaut $granularity minutes).
		// Un petit schéma vaut mieux. Si on a une granularité de 15 minutes (choix défaut pour l'ARAY)
		// Prenons un exemple fictif. Entre 7h et 11h du matin, avec :
		// un cours le lundi de 7h15 à 8h30,
		// un cours le mercredi de 13h à 14h
		// un cours le jeudi de 9h15 à 10h30,
		// un cours le vendredi de 9h45 à 11h,
		// rien les mardi, samedi
		// Une heure = 60 minutes = 4 unités de 15 minutes = 4 "bits"
		// Un bit prend la valeur 1 s'il y a un cours pendant l'unité de temps qu'il représente, 0 sinon
		// On combine les bits de même position pour chaque jour, par OU logique,
		// pour obtenir un "bitmask" pour la semaine
		// Ainsi:    | 7h    | 8h    | 9h    | 10h   | 11h  | 12h  | 13h  
		// Lundi:    | 0111  | 1100  | 0000  | 0000  | 0000 | 0000 | 0000
		// Mardi:    | 0000  | 0000  | 0000  | 0000  | 0000 | 0000 | 0000
		// Mercredi: | 0000  | 0000  | 0000  | 0000  | 0000 | 0000 | 1111
		// Jeudi:    | 0000  | 0000  | 1111  | 1100  | 0000 | 0000 | 0000
		// Vendredi: | 0000  | 0000  | 0001  | 1111  | 0000 | 0000 | 0000
		// Samedi:   | 0000  | 0000  | 0000  | 0000  | 0000 | 0000 | 0000

		// SEMAINE:  | 0111  | 1100  | 1111  | 1111  | 0000 | 0000 | 1111
		// On voit qu'on aura un intervalle vide entre 11h et 13h donc une "pause"
		// On peut avoir plusieurs pauses par jour bien sûr

		// On divise donc les bornes en minutes par la granularité. Par  on a pris 15 minutes
		// Donc ici pour heure_mini = 420 et granularite = 15, on obtiendra intStartLimit = 28
		// Idem, pour heure_maxi = 1320, granularite = 15, intStartLimit = 88
		// Si on revenait à l'exemple ci-dessus, on ne tient pas compte le matin des unités de temps
		// entre 0h et 7h, soit des 28 premières unités de la journée : on commence à compter à partir de 28,
		// et on s'arrête à 87 inclus.
		$intStartLimit = $heureMini / $granularity;
		$intStopLimit = $heureMaxi / $granularity;
		$intDuration = $intStopLimit - $intStartLimit;
		// Avec granularite = 15, bitsPerHour vaut 4 comme dans notre exemple
		$bitsPerHour = 60 / $granularity;
		
		// On doit partir avec un masque vide
		$mask = array();
		// On remplit donc tous ses bits de 0 (false)
		for( $i = 0; $i < $intDuration ; $i++ ) {
			$mask[$i] = false;
		}
		// On récupère les cours (classes en anglais)
		$classes = $this->getClasses();

		// Les cours sont ordonnés par:
		// clé jour => tableau de cours,
		// puis : clé cours => données du cours

		// Pour chaque jour
		foreach( $classes as $day => $classes ) {

			// Pour chaque cours de ce jour
			foreach( $classes as $slug => $class ) {
				// On calcule l'index du bit de debut du cours, par rapport à la borne de début.
				// Par exemple si le cours debute à 9h, soit 540 minutes, soit 36 bits, cela fait:
				// 36 - 28 = 8, le cours commence au bit 8 (voir cours jeudi ci-dessus)
				$hourBeginBit = $class['stHour'] / $granularity - $intStartLimit;
				// De même s'il finit à 10h30, cela fait 630 minutes, donc 42 bits.
				// 42 - 28 = 14. A noter que le bit 14 n'est pas inclus, car c'est le 1er bit
				// qui suit la fin du cours. Le cours comprend les bits 8 à 13.
				$hourEndBit = $class['endHour'] / $granularity - $intStartLimit;
				// Dans notre exemple on met les bits de 8 à 13 inclus à 1
				for( $index = $hourBeginBit; $index < $hourEndBit; $index++ ) {
					$mask[$index] = true;
				}
			}
		}
		// A la fin, on obtient le masque résultant de la combinaison de tous les cours,
		// tel que montré dans l'exemple. Mais ce n'est pas fini, ça va même se corser...
		
		// Copie de travail de nos limites calculées.
		// Ceci pour calculer les limites du masque, c'est à dire le plus petit et le plus grand offset
		// pour lesquels le bit = 1. Ces limites seront contenues dans loBoundary et hiBoundary
		// Si on reprend notre exemple pour une semaine... (complété avec cours en continu de 13h à 21h30).
		// Heure:    | 7h    | 8h    | 9h    | 10h   | 11h  | 12h  | 13h  | ...  | 21h  | 22h
		// Offset:   | 28    | 32    | 36    | 40    | 44   | 48   | 52   | ...  | 84   | 88
		// SEMAINE:  | 0111  | 1100  | 1111  | 1111  | 0000 | 0000 | 1111 | .... | 1100 |
		// On veut supprimer l'intervalle entre 7h et 7h15 (bit 28) et celui entre 21h30 et 22h
		// (bits 86-87). Cela nous donne de nouvelles bornes pour l'intervalle : de 29 à 85 inclus.
		// Cela nous permettra par la suite de compter le nombre de plages contenant un ou des 0,
		// dans cet intervalle (ici une plage de z&ros entre les bits 44 et 51 inclus).

		// Au début loBoundary est fixée à l'offset de FIN (88 dans notre exemple)
		$loBoundary = $intStopLimit; 	// lo pour lower
		// Et hiBoundary à l'offset de DEBUT (28 dans notre exemple)
		$hiBoundary = $intStartLimit;	// hi pour higher

		// Calculer les limites du masque
		// On itère à nouveau sur tous les bits (dans notre exemple, 28 à 87 inclus)
		for( $i = 0 ; $i < $intDuration ; $i++ ) {
			// Si le bit est à 1 et qu'on est avant la limite inférieure, on avance cette limite
			if( $mask[$i] && ( $i < $loBoundary ) ) {
				$loBoundary = $i;
			}
			// Si le bit est à 1 et qu'on est après la limite supérieure, on recule cette limite
			if( $mask[$i] && ( $i > $hiBoundary ) ) {
				$hiBoundary = $i;
			}
		}
		// A la fin on aura loBoundary = 29 et hiBoundary = 85 dans notre exemple

		// Le tableau intervalles va contenir une entrée pour chaque plage de zéros,
		// au sein de notre intervalle global recalculé
		$intervalles = array();
		// On initialise l'index $i avec la frontière basse
		// $int_deb et $int_fin contiendront les bits de début et fin de chaque intervalle
		// $num_ints le nombre d'intervalles trouvés
		$i = $loBoundary; $int_deb = 0 ; $int_fin = 0; $num_ints = 0;
		// echo "lo: ". $loBoundary . "<br>hi: " . $hiBoundary . "<br>";
		// On sait que quand cette boucle commence, le premier bit trouvé sera à 1
		while( $i <= $hiBoundary ) {
			// var_dump($mask);die();
			// On continue à incrémenter $i (voir $i++ en fin de boucle)
			// jusqu'à trouver un bit à zéro : on a trouvé une plage vide
			if( !$mask[$i] ) {
				// On positionne donc le début de la plage à l'index courant
				$int_deb = $i;
				// On incrémente l'index tant qu'on trouve des 0
				while( !$mask[$i] && $i <= $hiBoundary ) {
					$i++;
				}
				// Si le bit à l'offset $i est à 1, on s'arrête: $int_fin
				// est l'index, non pas du dernier bit de la plage de 0,
				// mais l'index du 1er bit à 1 qui suit cette plage
				$int_fin = $i;

				// On ne veut tenir compte que des heures COMPLETES de pause,
				// commençant et finissant sur des heures "rondes" (11h, 12h, 13h, etc.).
				// 2eme exemple: si on a pause entre 11h15 et 14h45, on tiendra compte seulement
				// des 2 heures entre 12 et 14. Pour cela, on fait des modulos.
				// Une pause de 11h15 à 14h45, cela signifie entre bits 45 et 58 inclus
				// (vérif: 3h30 = 14 bits pour une granularité de 15 toujours)

				// Ici modulo = 1
				$int_deb_mod = $int_deb % $bitsPerHour;
				// Si modulo non nul, on aligne le debut de la pause sur le debut de l'heure suivante:
				// On ajoute donc un nombre de bits suffisant pour arriver à un nombre divisible par
				// bitsPerHour.
				// Ex pour 45, modulo = 1, on ajoute 4 - modulo, on obtient 48
				if( $int_deb_mod > 0 ) $int_deb += ( $bitsPerHour - $int_deb_mod );
				// On divise par bitsPerHour (ici 4) et on obtient 12h pour l'heure de début
				$int_deb /= $bitsPerHour;

				// Idem pour fin de la pause. Ici 59 % 4 = 3 (59 = index du 1er bit à 1 suivant la pause)
				$int_fin_mod = $int_fin % $bitsPerHour;
				// On retranche le modulo pour obtenir un nombre "rond" (59 - 3 = 56, bien divisible par 4)
				if( $int_fin_mod > 0 ) $int_fin -= $int_fin_mod;
				// Heure de fin = 56 / 4 = 14h
				$int_fin /= $bitsPerHour;

				// Longueur de la pause = 2h toujours dans notre 2ème exemple
				$int_len = $int_fin - $int_deb;
				// Si la longueur de la pause vaut 0 heure complète, on l'ignore
				// (cas où on a une pause de 30 min. entre 11h15 et 11h45 par exemple)
				if( $int_len == 0 ) continue;
				
				// Sinon on ajoute cet intervalle à notre liste: son debut, sa fin, sa duree
				$intervalles[] = array( 'debut' => $int_deb, 'fin' => $int_fin, 'duree' => $int_len );
				// On incremente le compteur de pauses trouvées
				$num_ints++;
			}
			// Increment de l'index
			$i++;
		}

		// On aura besoin pour le rendu du tableau, de connaitre le nombre total d'heures complètes de pause
		// (càd, on le répète, alignées sur des heures "rondes") trouvées dans la journée
		// On additionne donc les durees de tous les intervalles
		$total_heures_pause = 0;
		foreach( $intervalles as $intervalle ) {
			$total_heures_pause += $intervalle['duree'];
		}
		
		// On renvoie le tableau d'intervalles, le nb. d'heures de pause, le bitmask, le nb de bits par heure
		return array( $intervalles, $total_heures_pause, $mask, $bitsPerHour ); 
	}

}
// Fin de la classeWP_YogaTimeTable_Templater


/**
 * 
 * Utility function to echo strings
 * @param string $str	string to echo
 * @param int $level	title level
 */
function customEcho( $str, $level = 0 ) {
	$openTag = $level > 0 ? "<h{$level}>" : "";
	$closeTag = $level > 0 ? "</h{$level}>" : "";
	echo "{$openTag}{$str}{$closeTag}<br>";
}

/**
 * 
 * Dump an array in HTML readable form
 * @param unknown_type $mixed	array to dump
 * @param unknown_type $level	recursion level
 */
function customDump( $mixed, $level = 0 ) {
	$i = 0;
	$indent = $level * 20;
	if( is_array( $mixed ) )
		foreach( $mixed as $key => $val ) {
			echo "<span style='margin-left: {$indent}px;'>";
			echo "k:$key => v:";
			if( is_array( $val ) ) {
				echo "<br>"; customDump( $val, $level + 1 );
			}
			else echo "$val<br>";
			echo "</span><br>";
		}
	else {
		echo "<span style='margin-left: {$indent}px;'>";
		echo "$i -> $mixed<br>";
		echo "</span><br>";
	}
}
?>