<?php

/*************************
 * HELPER FUNCTIONS      
 * Par exemple:
 * generic_select: créer un <select> de façon générique à partir de données générées dans les wpytt-data-functions.php
 *************************/
class WP_Plugin_FormHelper {

	private static $_cpIndex = 0;
	
	public static $_functions_args_mappings = array(
		'generic_select' => 	array( 'option', 'name', 'selected', 'data_src', 'src_column' ),
		'generic_checkbox' => 	array( 'option', 'name', 'selected', 'data_src', 'src_column' ),
		'generic_radio' => 		array( 'option', 'name', 'selected', 'data_src' ),
		'generic_textarea' => 	array( 'option', 'name', 'selected' ),
		'gen_color_picker' =>	array( 'option', 'name', 'sub_name', 'selected' ) //,
		//'gen_box_model' =>		array( 'option', 'name', 'sub_name', 'selected', 'data_src' )
	);
		
	private $_field_helpers_args = array(	
		'generic_select' => 5,
		'generic_checkbox' => 5,
		'generic_radio' => 4,
		'generic_textarea' => 3
	);
	
	public static function getArgs( $function ) {
		return self::$_functions_args_mappings[$function];
	}

	public static function gen_color_picker( $option, $name, $sub_name, $value ) {
		$field_id = 'colorpickerField-' . self::$_cpIndex;
		self::$_cpIndex++;
		$style = !empty( $value ) ? "style='background-color: #$value'" : "";
		$field_name = $option . "[$name]";
		$field_name .= !empty( $sub_name ) ? "[$sub_name]" : '';
		
		echo "\n\t<input type='text' size='6' id='$field_id' name='$field_name' class='colorpickerField' value='$value' $style />\n";
	}
	
/*	public static function gen_box_model( $option, $name, $sub_name, $value ) {
		echo "$option $name $sub_name $value <br>";
		echo self::generic_textarea( $option, $name . '[' . $sub_name . ']', $value );
	}
*/
	static function pretty_print_heure( $heure_numerique ) {
			 $heure_lisible_h = $heure_numerique / 60;
			 $heure_lisible_m = $heure_numerique % 60;
			 return sprintf( "%2d:%02d", $heure_lisible_h, $heure_lisible_m );
	}

	static function select_nombres( $min, $max, $granularite = 1 ) {
		$tab = array();
		for( $i = $min ; $i <= $max ; $i += $granularite ) $tab[$i] = $i;
		return $tab;
	}

	static function select_nombres_float( $min, $max, $granularite = 1 ) {
		$tab = array();
		for( $i = $min ; $i <= $max ; $i += $granularite ) $tab[] = $i;
		return $tab;
	}
	static function get_tailles_police() {
		return self::select_nombres_float( 8, 16, 0.5 );
	}

	/* Genere un tableau de choix d'heure */
	static function select_heures( $limite_basse, $limite_haute, $granularite ) {
		
		if( $limite_basse < 0 || $limite_haute > 24 || $granularite > 60 || ( $granularite % 5) ) return null;
		
		$num_tranches = 60 / $granularite;
		
		$tranche_basse = $limite_basse * $num_tranches;
		$tranche_haute = $limite_haute * $num_tranches;
		$heures = array();
		for( $tranche = $tranche_basse ; $tranche <= $tranche_haute; $tranche++ ) {
			 $cle_heure = $tranche * $granularite;
			 $heure_lisible_h = $cle_heure / 60;
			 $heure_lisible_m = $cle_heure % 60;
			 $heure_lisible = sprintf( "%02dh%02d", $heure_lisible_h, $heure_lisible_m );
			 $heures[$cle_heure] = $heure_lisible;
		}
		
		return $heures;
	}

	static function generic_select( $option, $slug, $title, $selected, $data, $object_column, $recompute = true, $wrap = false, $class='' ) { 
		$recompute = ( $recompute ) ? " onchange='recompute();'" : "";
		$output = ( ($wrap) ? "<tr>" : "" ) . "<td><label for='$slug'>$title</label></td>\n";
		$output .= "\t<td><select id='$slug' class='$class' name='{$option}[{$slug}]'$recompute style='margin-left: 1.8em; margin-bottom: 0.8em;'>";
		foreach( $data as $key => $value ) {
			if( is_object( $value ) ) { 
				$object_vars = get_object_vars( $value );
				$value = $object_vars[$object_column];
			}
			$selected_flag = ( $selected == $key ) ? " selected='selected'" : "";
			$output .= "\t<option value='$key'$selected_flag>$value</option>\n";
		}
		$output .= "</select></td>".( ($wrap) ? "</tr>" : "" )."\n";
		return $output;
	}

	static function generic_radio( $option, $group, $checked, $slug_title_kva ) {
		$output = "";
		foreach( $slug_title_kva as $slug => $title ) {
			$checked_flag = ( $checked == $slug ) ? " checked='checked'" : "";
			$output .= "<input type='radio' style='margin-bottom: 6px;' name='{$option}[$group]' value='$slug'$checked_flag>&nbsp;$title</input><br>";
		}
		return $output;
	}


	static function generic_checkbox( $option, $slug, $title, $checked, $data, $object_column ) {

		$output = "<td><label for='$slug'>$title</label></td>\n";
		foreach( $data as $key => $value ) {
			if( is_object( $value ) ) { 
				$object_vars = get_object_vars( $value );
				$value = $object_vars[$object_column];
			}
			 $checked_flag = ( !empty( $checked ) && in_array( $key, $checked ) ) ? " checked='checked'" : "";
			$output .= "\t<td><input type='checkbox' name='{$option}[{$slug}][$key]' value='$key'{$checked_flag}/>&nbsp;$value&nbsp;</td>\n";
		}
		return $output;
	}

	static function generic_textarea( $option, $slug, $modele_custom ) {
		$output = "<textarea name='{$option}[{$slug}]' rows='3' style='margin-left: 1.8em; width: 90%;'>";
		$echo_custom = !empty( $modele_custom );
		$output .= $echo_custom ? $modele_custom : '$niv : $debut à $fin'."\n".'$prof'; 
		$output .= "</textarea>";
		return $output;
	}


	static function wpytt_get_niveaux( $order = 'ASC', $orderby = 'term_id' ) {
        global $wpdb;
		$args = array( 'hide_empty' => false, 'orderby' => 'slug', 'order' => $order );
        $orderClause = " ORDER BY $orderby $order";
        $termTaxRows = $wpdb->get_results( "SELECT term_id,taxonomy FROM $wpdb->term_taxonomy WHERE taxonomy='niveau-cours'");
        $termIds = array();
        foreach( $termTaxRows as $termTaxRow ) {
            $termIds[] = $termTaxRow->term_id;
        }
        if(empty($termIds)) return [];
        $tax_terms = $wpdb->get_results( "SELECT t.term_id,t.name,t.slug,tt.taxonomy,tt.description FROM $wpdb->terms t, $wpdb->term_taxonomy tt WHERE t.term_id = tt.term_id AND t.term_id IN (" . implode(',', $termIds) . ") $orderClause" );

		return $tax_terms;
	}

	static function wpytt_get_niveaux_kva( $default = 'None') {
		$terms_kva = array();
		$terms = self::wpytt_get_niveaux( 'ASC' );
		foreach( $terms as $term ) $terms_kva[$term->slug] = $term->description;
		return $terms_kva;
	}




	/* This is meant to update plugin options while we're not in the options page, e.g. from midday gap recompute */
	static function wpytt_options_update() {
		global $wpytt_options;
		update_option( 'wpytt_options', $wpytt_options );
	}

	static function wpytt_get_options() {
		global $wpytt_options;
		$wpytt_options = get_option( 'wpytt_options' ) ;
	}

}

?>