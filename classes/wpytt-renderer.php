<?php
/**
 * 
 * Table renderer interface
 * @author Benoit
 *
 */
abstract class WP_YogaTimeTable_Renderer {

	protected $_templater;
	protected $_days;

	abstract public function renderHeader();
	abstract public function renderFooter();
	abstract public function renderHeadColumn();
	abstract public function renderColumn( $day );

	public function render( $echo = true ) {
	
		// Initialisation
		if( method_exists( $this, 'renderInit' ) ) {
			$this->renderInit();
		}

		// Récupérer les jours "actifs", càd cochés dans la page d'options
		$this->_days = $this->_templater->getDays();

		// Ouvrir la div encadrant la table
		$out = $this->renderHeader();

		// Rendu de la 1ere colonne : heures du jour
		$out .= $this->renderHeadColumn();

		// Pour chaque jour : rendu de la colonne correspondante
		foreach( array_keys( $this->_days ) as $dayCode ) {
			$out .= $this->renderColumn( $dayCode );
		}
		
		// Fermer la div encadrant la table
		$out .= $this->renderFooter();

		// Si on ne choisit pas un affichage direct, on retourne le contenu
		if( !$echo ) return $out;
		echo $out;
	}
	
}
?>