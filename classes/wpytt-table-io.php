<?php
/******************************************************
 * "Entrées/Sorties" pour les tableaux HTML générés   *
 * par le renderer                                    *
 ******************************************************/

/**
 * Interface commune qui va être implémentée par deux classes :
 * WPYTT_FileTableIO et WPYTT_DBaseTableIO
 */
interface WPYTT_TableIO {
	function writeTable( $post_type, $table );
	function readTable( $post_type );
}

/**
 * Classe pour la lecture/écriture d'un tableau HTML de/vers le disque 
 */
class WPYTT_FileTableIO implements WPYTT_TableIO {
	
	private $_filePath;
	private $_fileName;
	
	/**
	 * Doit être construite avec un path et nom de fichier en paramètre
	 */
	function __construct( $path, $file ) {
		$this->_filePath = $path;
		$this->_fileName = $file;
	}
	
	/**
	 * Ecriture sur disque
	 */
	function writeTable( $post_type, $table ) {
		$fh = fopen( $this->_filePath . $this->_fileName, 'w+' );
		if( !$fh ) die( 'die on file open' );
		$fw = fwrite( $fh, $table );
		if( !$fw ) die( 'die on file write' );
		$fcSuccess = fclose( $fh );
	}

	/**
	 * Handler d'erreur, appelé en cas d'échec de la lecture dans readTable()
	 */
	function fileErrorHandler($errno, $errstr, $errfile, $errline) {
		die('I/O error');
	}
	
	/**
	 * Lecture d'une table sur le disque
	 */
	function readTable( $post_type ) {
		// Installation du handleur d'erreurs
		$oldHandler = set_error_handler( array( $this, 'fileErrorHandler' ) );
		// Ouverture et lecture du fichier dans une string
		$fileName = $this->_filePath . $this->_fileName;
		$fh = fopen( $fileName, 'r' );
		$contents = fread( $fh, filesize( $fileName ) );
		$fcSuccess = fclose( $fh );
		// Restauration du handler d'erreurs par défaut
		restore_error_handler();

		// Retourne le contenu du tableau si succès, false autrement
		return ( !$fh || empty( $contents ) || !$wpytSuccess ) ? false : $contents;
	}

}

/**
 * Classe pour la lecture/écriture d'un tableau HTML vers la DB
 */
class WPYTT_DBaseTableIO implements WPYTT_TableIO {
	
	private $_filePath;
	private $_fileName;
	
	/**
	 * Rien a faire dans le constructeur !
	 */
	function __construct() {
	}
	
	/**
	 * Ecriture dans la table sous la forme d'un "post" avec le type "time_table"
	 * A noter, on n'a pas enregistré ce type comme Custom Post Type.
	 * On pourrait, mais ça n'aurait pas un grand intérêt !
	 */
	function writeTable( $post_type, $table ) {
		// Créer l'objet post avec pour contenu, le code HTML de la table
		  $tablePost = array(
			 'post_title' => $post_type . ' ' . date('Y-m-d H:i'),
			 'post_status' => 'publish',
			 'post_author' => wp_get_current_user()->ID,
			 'post_content' => $table,
			 'post_type' => $post_type
		  );

		// Insérer le tableau dans la base
		wp_insert_post( $tablePost );
	}


	/**
	 * Lecture d'une table depuis la DB
	 */
	function readTable( $post_type ) {
		// On ne prend UN post de type time_table, publié, le plus récent
		$tablePosts = get_posts( array( 
			'post_type' => $post_type, 'numberposts' => 1, 'post_status' => 'publish', 
			'orderby' => 'post_date', 'order' => 'DESC' ) );
		if( empty( $tablePosts ) ) return 'Not found !';
		$tablePost = $tablePosts[0];
		// On renvoie le contenu du post : c'est notre tableau HTML
		return $tablePost->post_content;
	}

}
?>