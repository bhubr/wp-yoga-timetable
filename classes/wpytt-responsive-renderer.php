<?php
/**
 * Le renderer au format HTML.
 * Le tableau est fait de plusieurs parties :
 * - Une entête contenant des "contrôles", c'est à dire 
 *   des checkboxes permettant de filtrer la liste des
 *   cours selon les professeurs ou les niveaux.
 * - Le tableau proprement dit, avec la colonne à 
 *   gauche pour les heures du jour, puis les autres
 *   colonnes correspondant chacune à un jour.
 * Il y a quelques subtilités pour obtenir un rendu correct du HTML :
 * Notamment, il ne faut pas qu'il y ait d'espaces entre les cases et entre les colonnes !
 * C'est la raison d'être des <!-- en fin de ligne et --> en début de ligne. Ils nous
 * d'encapsuler des espaces dans le code HTML pour l'aérer, mais ces espaces sont commentés,
 * donc non pris en compte par le moteur de rendu du navigateur.
 *
 * ATTENTION à bien regarder le code de la classe parente dans wpytt-renderer.php
 * pour voir l'ordre dans lequel se font les étapes du "rendu"
 */

class WP_YogaTimeTable_ResponsiveRenderer extends WP_YogaTimeTable_Renderer {

  // L'objet principal du plugin
  protected $_wpytt;
  // Les options générales du plugin (cf page options)
  protected $_opts;
  // Les options d'apparence du tableau (cf page options correspondante)
  protected $_css;
  protected $_teachersControlsData;
  protected $_levelsControlsData;
  // Les quatre paramètres suivants vont nous permettre de calculer
  // les intervalles de pause pour les afficher de manière particulière
  protected $_intervals;
  protected $_breakHours;
  protected $_intervalsMask;
  protected $_bitsPerHour;
  protected $_days;

  protected $models = array(
  // link open, levels string, start hour pretty, end hour pretty, link close, teacher
  // "standard" => '',
  // link open, levels string, start hour pretty, end hour pretty, link close, teacher
  "multiple" => '',
  "court" => "<div>{{{linkOpen}}}{{firstLevelString}} : fin {{endHourPr}}{{{linkClose}}}</div>",
  // link open, levels string, start hour pretty, end hour pretty, link close, teacher
  "multiple_inv" => array(
    "string" => "<div class='prof '>%s</div><div class='niv %s'>%s</div><div class='class-hr'>{linkOpen}%s &mdash; %s{linkClose}</div>%s",
    "args"   => array( 'teacherNm', 'levelSlugs', 'firstLevelString', 'stHourPr', 'endHourPr' )
  ),
  "court_inv" => "{{linkOpen}}{{firstLevelString}} : fin {{endHourPr}}{{linkClose}}",
  "custom" => array(
    "string" => "{{linkOpen}}%s{{linkClose}}",
    "args"   => array( 'modelCust' )
  )
);
  
  function __construct( WP_YogaTimeTable_Templater $templater, $wpytt, $wpyttOpts, $wpyttCss ) {
    $this->_templater = $templater;
    $this->_wpytt = $wpytt;
    $this->_opts = $wpyttOpts;
    $this->_css = $wpyttCss;
    // Récupérer les contrôles pour les profs et les niveaux
    $this->_teachersControlsData = $this->_templater->getGenericBitmaskAndControls( $this->_templater->getTeachers(), 'cbprof' );
    $this->_levelsControlsData = $this->_templater->getGenericBitmaskAndControls( WP_Plugin_FormHelper::wpytt_get_niveaux_kva(''), 'cbniveau' );
    $this->_isAdmin = current_user_can( 'manage_options' );
  }
        
    // Before rendering table to DB
    function setCss( $css ) {
        $this->_css = $css;
    }
  
  /**
   * Initialisation avant rendu
   * Consiste surtout en l'appel à templater->computeGaps
   * qui permet de calculer les intervales entre chaque cours
   */ 
  function renderInit() {
    /* Get intervals */
    list( $intervals, $breakHours, $mask, $bph ) = $this->_templater->computeGaps();
    $this->_intervals = $intervals;
    $this->_breakHours = $breakHours;
    $this->_intervalsMask = $mask;
    $this->_bitsPerHour = $bph;
  }
  
  /**
   * Rendu de l'en-tête, incluant les contrôles et l'ouverture d'une div
   */
  function renderHeader() {
    $out = $this->renderControls();
    $out .= '<div class="vtt-box-resp">';
    return $out;
  }
  
  /**
   * Rendu des "contrôles" du tableau : checkboxes permettant d'afficher/cacher les cours
   * associés à tel ou tel prof, et les cours de tel ou tel niveau.
   */
  function renderControls() {
    return "";
/*    $controls = "\n\n<div style='padding: 4px; font-size: 11px;'><span class='cb-row-head'>Professeurs&nbsp;:</span>\n";
    $controls .= implode( "\n", $this->_teachersControlsData[1] );
    $teachersMaskAll = $this->_teachersControlsData[0]['__ALL__'];

    // On se servira de la valeur cachée #pmask dans le jQuery ou mootools qui contrôle l'interactivité
    $controls .= "<br><input type='hidden' id='pmask' name='pmask' value='$teachersMaskAll' />\n" .
      "<span class='cb-row-head'>Niveaux&nbsp;:</span>\n";
    $controls .= implode( "\n", $this->_levelsControlsData[1] );
    $levelsMaskAll = $this->_levelsControlsData[0]['__ALL__'];
    
    // Idem pour #nmask
    $controls .= "<br><input type='hidden' id='nmask' name='nmask' value='$levelsMaskAll' />\n";
    
    return $controls;*/
  }
  
  /**
   * Effecuter le rendu de la 1ere colonne, avec les heures du jour.
   */
  function renderHeadColumn() {
    return "";
  }

  function linkOpen($id) {
    return $this->_isAdmin ? '<a target="_blank" href="' . get_edit_post_link( $id ) . '">' : '';
  }

  function linkClose() {
    return $this->_isAdmin ? "</a>" : "";
  }
  
  /**
   * Output a table column, corresponding to a day of the week
   * @param day the day of week
   */
  function renderColumn( $day ) {
  
    $dayPretty = $this->_days[$day];

    $output = '<div><div class="tt-resp-day">' .$dayPretty .'</div><div class="tt-resp-table">';

    $isAdmin = current_user_can( 'manage_options' );

    $classesByDay = $this->_templater->getClasses();

    $days = $this->_templater->getDays();
    $classes = $classesByDay[$day];
    //$idx = 0;
    $previousClass = null;

    $k = 0;
    $m = new Mustache_Engine(array(
      'loader' => new Mustache_Loader_FilesystemLoader(realpath(__DIR__) . '/../templates/responsive')
    ));
    $classesKeys = array_keys($classes);
    while($k < count($classesKeys) ) {
      $classSlug = $classesKeys[$k];
      $classData = $classes[$classSlug];
    //foreach( $classes as $classSlug => $classData ) {
      //$output .= ' --> '. print_r($classData, true) . "<--\n";
      extract( $classData );
      list( $levels, $levelsString, $firstLevelString, $levelSlugs, $levelsMask ) = $this->_templater->getTermNiveaux( $id );
      $classData['levelSlugs'] = $levelSlugs;
      $classData['levelsString'] = $levelsString;
      $classData['firstLevelString'] = $firstLevelString;
      $classData['linkOpen'] = $this->linkOpen($id);
      $classData['linkClose'] = $this->linkClose();

      $teachersMask = $this->_teachersControlsData[0][$teacherSl];
      $intervals = $this->_intervals;
      $breakHrs = $this->_breakHours;
                        
      // Calcule de la durée en minutes du cours
      $duration = $endHour - $stHour;

      if( empty( $model ) ) {
          $model = 'standard';
      }

      // récupérer le "template" de case en fonction du modèle

      $nextClasses = [];
      $knext = $k + 1;
      $nextSlug = $classesKeys[$knext];
      $nextData = $classes[$nextSlug];
      if($model === 'multiple') {
        $subclasses = array($classData);
        while( $nextData['model'] === 'court' ) {
          list( $a, $b, $nFirstLevelString, $levelSlugs, $e ) = $this->_templater->getTermNiveaux( $nextData['id'] );
          $subclasses[] = array(
            'stHourPr' => $classData['stHourPr'],
            'endHourPr' => $nextData['endHourPr'],
            'firstLevelString' => $nFirstLevelString,
            'levelSlugs' => $levelSlugs,
            'linkOpen' => $this->linkOpen( $nextData['id'] ),
            'linkClose' => $this->linkClose()
          );
          $knext++;
          $k++;
          $nextSlug = $classesKeys[$knext];
          $nextData = $classes[$nextSlug];
        }
        $boxContent = $m->render($model, array(
          'classes' => $subclasses,
          'teacherNm' => $classData['teacherNm']
        ));
        $output .= $boxContent;
      }
      else if($model === 'court_inv') {
        $subclasses = array($classData);
        while( $nextData['model'] === 'court_inv' || $nextData['model'] === 'multiple_inv' ) {
          list( $a, $b, $nFirstLevelString, $levelSlugs, $e ) = $this->_templater->getTermNiveaux( $nextData['id'] );
          $subclasses[] = array(
            'stHourPr' => $nextData['stHourPr'],
            'endHourPr' => $nextData['endHourPr'],
            'firstLevelString' => $nFirstLevelString,
            'levelSlugs' => $levelSlugs,
            'linkOpen' => $this->linkOpen( $nextData['id'] ),
            'linkClose' => $this->linkClose()
          );
          $knext++;
          $k++;
          $nextSlug = $classesKeys[$knext];
          $nextData = $classes[$nextSlug];
        }
        $lastIdx = count($subclasses) - 1;
        for($idx = 0 ; $idx < $lastIdx ; $idx++) {
          $subclasses[$idx]['endHourPr'] = $subclasses[$lastIdx]['endHourPr'];
        }

        $boxContent = $m->render('multiple', array(
          'classes' => $subclasses,
          'teacherNm' => $classData['teacherNm']
        ));
        $output .= $boxContent;
      }


      $boxModel = $this->models[$model];
      if(array_search($model, array('standard')) !== false) {
        $boxContent = $m->render($model, $classData);
        $output .= $boxContent;
      }

      // $boxContent = vsprintf( $boxModel['string'], $boxParams );

      
      // Si on est admin on va remplacer la 1ère ligne (avec niveau et heure)
      // par un lien cliquable pour aller directement dans la partie admin éditer ce cours.
      // un bon raccourci pour modifier directement à partir d'une observation faite dans le tableau
      if( $model == 'custom' ) {
          $boxContent = str_replace( '$niv', $levelsString, $boxContent );
          //$fin = WP_Plugin_FormHelper::pretty_print_heure
          $boxContent = str_replace( '$debut', $stHourPr, $boxContent );
          $boxContent = str_replace( '$fin', $endHourPr, $boxContent );
          $boxContent = str_replace( '$prof', $teacherNm, $boxContent );
      }
      $boxContent = str_replace( '{linkOpen}', $linkOpen, $boxContent );
      $boxContent = str_replace( '{linkClose}', $linkClose, $boxContent );
      
      // Ajouter cette case avec ses propriétés de style a l'output de la colonne
      // A noter: teacherSl et levelSlugs ça va par exemple être "luc-carimalo" et "n3 n4 n5"
      // On stocke le masque du prof et les masques de niveaux cumulés correspondant à ce cours
      // Ils seront manipulés par le JS.
      // Chaque checkbox de contrôle (ex. la checkbox correspondant à Yann) a un "poids", un "bitmask"
      // par exemple 1 pour Yann. Si le cours a pour prof associé Yann, son $teachersMask va valloir 1
      // On affiche la case du cours si la checkbox correspondant à son teachersMask est cochée

      // $output .= "<div class='xclass $teacherSl $xlevelSlugs' style='$style'"
      //     . "><input type='hidden' value='$teachersMask|$levelsMask' /><div class='class-text'>"
      //     . $boxContent
      //     . "</div></div>";
      $previousClass = $classData;
      
      $k++;
    }
    $output .= "</div></div>\n\n";
    // echo htmlentities($output);
    return $output;
  }
  
  function renderIntervalsColumn() {
    $out = "<div class='vday vday-'><!--"
       . "\t--><div class='vyclass colhead'>Mask</div><!--\n";

    //list( $intervals, $breakHrs, $mask, $bph ) = $this->_templater->computeGaps();
    $mask = $this->_intervalsMask;
    $bph = $this->_bitsPerHour;
    $bphm = $bph - 1;
    $bitHeightPx = floor( $this->_css['hour_height'] / $bph );
    $i = 0;
    foreach( $mask as $bit ) {
      $out .= $i % $bph == 0 ? "--><div class='vhour' style='border-bottom: 1px solid gray;'>" : "";
      
      $color = $bit ? 'green' : 'gray';
      $out .= "<div style='height: {$bitHeightPx}px; width: 100%; background: $color;'></div>" ;
      $out .= $i % $bph == $bphm ? "</div><!--" : "";
      $i++;
      }
    $out .= "--></div>\n\n";
    return $out;
  }

  /**
   * Fermeture des div ouvertes précdemment
   */
  function renderFooter() {
    return "</div>\n<!-- vtt-box -->\n";
  }


}
?>