<?php
class WP_YogaTimeTable_Tarifs_Handler {

	public function __construct() {
		add_action( 'niveau-cours_add_form_fields', array( $this, 'addNiveauFormFields' ), 10, 2 );
		add_action( 'niveau-cours_edit_form_fields', array( $this, 'addNiveauFormFields' ), 10, 2 );
		add_action( 'created_niveau-cours', array( $this, 'saveNiveauCours' ) );
		add_action( 'edited_niveau-cours', array( $this, 'saveNiveauCours' ) );
		$this->m = new Mustache_Engine(
			array(
				'loader' => new Mustache_Loader_FilesystemLoader(
					realpath(__DIR__) . '/../templates'
				)
			)
		);
	}

	function get_tarifs_for_niveau( $term_id ) {
		$tarifs = get_term_meta( $term_id, 'niveau_tarif', true );
		if( empty( $tarifs ) ) {
			$tarifs = array(
			'1trim' => array( 'normal' => '', 'reduit' => '' ),
			'2trim' => array( 'normal' => '', 'reduit' => '' ),
			'3trim' => array( 'normal' => '', 'reduit' => '' )
			);
		}
		return $tarifs;
	}

	function get_meta_for_niveau( $term_id, $meta ) {
		$meta_value = get_term_meta( $term_id, $meta, true );
		return ! empty( $meta_value ) ? $meta_value : '';
	}


	function addNiveauFormFields($term) {
		$tarifs = $this->get_tarifs_for_niveau( $term->term_id );
		$duree = $this->get_meta_for_niveau( $term->term_id, 'niveau_duree' );
		$tarif_legende = $this->get_meta_for_niveau( $term->term_id, 'tarif_legende' );
		$ordre_niveau = $this->get_meta_for_niveau( $term->term_id, 'ordre_niveau' );
		?>
			<table class="form-table">
			<tr class="form-field">
				<th for="ordre-niveau" scope="row">
					<label>Ordre</label>
				</th>
				<td>
					<input type="text" name="ordre-niveau" value="<?php echo $ordre_niveau; ?>" size="4" />
					<p class="description">Ordre d'affichage des lignes dans le tableau</p>
				</td>
			</tr>
			<tr class="form-field">
				<th for="cours-duree" scope="row">
					<label>Durée</label>
				</th>
				<td>
					<input type="text" name="duree" value="<?php echo $duree; ?>" size="4" />
					<p class="description">Durée d'un cours de ce niveau</p>
				</td>
			</tr>
			<tr class="form-field">
				<th for="tarif-legende" scope="row">
					<label>Légende</label>
				</th>
				<td>
					<input type="text" name="tarif-legende" value="<?php echo $tarif_legende; ?>" size="4" />
					<p class="description">Légende à afficher après le tarif (par ex. pour atelier méditation)</p>
				</td>
			</tr>
			<tr class="form-field">
				<th scope="row">
					<label>Tarifs</label>
				</th>
				<td>
					<table>
					<tr>
						<th></th>
						<th>Tarif normal</th>
						<th>Tarif réduit</th>
					<tr>
						<th>1&nbsp;trimestre</th>
						<td><input type="text" name="tarif[1trim][normal]" size="6" value="<?php echo $tarifs['1trim']['normal']; ?>" /></td>
						<td><input type="text" name="tarif[1trim][reduit]" size="6" value="<?php echo $tarifs['1trim']['reduit']; ?>" /></td>
					</tr>
					<tr>
						<th>2&nbsp;trimestres</th>
						<td><input type="text" name="tarif[2trim][normal]" size="6" value="<?php echo $tarifs['2trim']['normal']; ?>" /></td>
						<td><input type="text" name="tarif[2trim][reduit]" size="6" value="<?php echo $tarifs['2trim']['reduit']; ?>" /></td>
					</tr>
					<tr>
						<th>3&nbsp;trimestres</th>
						<td><input type="text" name="tarif[3trim][normal]" size="6" value="<?php echo $tarifs['3trim']['normal']; ?>" /></td>
						<td><input type="text" name="tarif[3trim][reduit]" size="6" value="<?php echo $tarifs['3trim']['reduit']; ?>" /></td>
					</tr>
					</table>
				</td>
			</tr>
		</table><?php
	}

	function saveNiveauCours($term_id) {
		update_term_meta( $term_id, 'niveau_tarif', $_POST['tarif'] );
		update_term_meta( $term_id, 'niveau_duree', $_POST['duree'] );
		update_term_meta( $term_id, 'tarif_legende', $_POST['tarif-legende'] );
		update_term_meta( $term_id, 'ordre_niveau', $_POST['ordre-niveau'] );
		$this->generateTable();
	}

	function generateTable() {
		$niveaux = $this->getTableData();
		$table = '<div class="wpytt-table-lg">' .
			$this->renderTable('desktop', $niveaux) .
			'</div><div class="wpytt-table-sm">' .
			$this->renderTable('responsive', $niveaux) .
			'</div>';
		$dbIo = new WPYTT_DBaseTableIO();
		$dbIo->writeTable( 'tarifs_table', $table );
	}

	function render() {
		$dbIo = new WPYTT_DBaseTableIO();
		return $dbIo->readTable( 'tarifs_table' );
	}

	function getTableData() {
		$terms = get_terms( 'niveau-cours', array(
		    'hide_empty' => false,
		) );
		$niveaux = array_map( function( $term ) {
			$all_tarifs = $this->get_tarifs_for_niveau( $term->term_id );
			$legende = $this->get_meta_for_niveau( $term->term_id, 'tarif_legende' );
			foreach( $all_tarifs as $trim => $trim_tarifs ) {
				foreach( ['normal', 'reduit'] as $tarif ) {
					$all_tarifs[$trim][$tarif] = empty( $trim_tarifs[$tarif] ) ?
						'&mdash;' : $trim_tarifs[$tarif] . ' €';
					$all_tarifs[$trim]['has_legende'] = ! empty( $trim_tarifs[$tarif] ) && ! empty( $legende );
					$all_tarifs[$trim]['has_tarif'] = ! empty( $trim_tarifs[$tarif] );
				}
			}
			return array(
				'description' => $term->description,
				'slug' => $term->slug,
				'duree' => $this->get_meta_for_niveau( $term->term_id, 'niveau_duree' ),
				'legende' => str_replace( ' ', '&nbsp;', $legende ),
				'tarifs' => $all_tarifs,
				'ordre' => (int)$this->get_meta_for_niveau( $term->term_id, 'ordre_niveau' )
			);
		}, $terms );
		usort( $niveaux, function( $niveau1, $niveau2 ) {
			if ($niveau1['ordre'] === $niveau2['ordre']) {
			    return 0;
			}
			return ($niveau1['ordre'] < $niveau2['ordre']) ? -1 : 1;
		} );
		return $niveaux;
	}

	function renderTable( $mode, $niveaux ) {
		return $this->m->render( "$mode/tarifs-table", array( 'niveaux' => $niveaux ) );
	}
}