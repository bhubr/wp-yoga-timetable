<?php
/**
 * Le renderer au format HTML.
 * Le tableau est fait de plusieurs parties :
 * - Une entête contenant des "contrôles", c'est à dire 
 *   des checkboxes permettant de filtrer la liste des
 *   cours selon les professeurs ou les niveaux.
 * - Le tableau proprement dit, avec la colonne à 
 *   gauche pour les heures du jour, puis les autres
 *   colonnes correspondant chacune à un jour.
 * Il y a quelques subtilités pour obtenir un rendu correct du HTML :
 * Notamment, il ne faut pas qu'il y ait d'espaces entre les cases et entre les colonnes !
 * C'est la raison d'être des <!-- en fin de ligne et --> en début de ligne. Ils nous
 * d'encapsuler des espaces dans le code HTML pour l'aérer, mais ces espaces sont commentés,
 * donc non pris en compte par le moteur de rendu du navigateur.
 *
 * ATTENTION à bien regarder le code de la classe parente dans wpytt-renderer.php
 * pour voir l'ordre dans lequel se font les étapes du "rendu"
 */

class WP_YogaTimeTable_HtmlRenderer extends WP_YogaTimeTable_Renderer {

	// L'objet principal du plugin
	protected $_wpytt;
	// Les options générales du plugin (cf page options)
	protected $_opts;
	// Les options d'apparence du tableau (cf page options correspondante)
	protected $_css;
	protected $_teachersControlsData;
	protected $_levelsControlsData;
	// Les quatre paramètres suivants vont nous permettre de calculer
	// les intervalles de pause pour les afficher de manière particulière
	protected $_intervals;
	protected $_breakHours;
	protected $_intervalsMask;
	protected $_bitsPerHour;
	protected $_days;
	
	function __construct( $templater, $wpytt, $wpyttOpts, $wpyttCss ) {
		$this->_templater = $templater;
		$this->_wpytt = $wpytt;
		$this->_opts = $wpyttOpts;
		$this->_css = $wpyttCss;

		// Récupérer les contrôles pour les profs et les niveaux
		$this->_teachersControlsData = $this->_templater->getGenericBitmaskAndControls( $this->_templater->getTeachers(), 'cbprof' );
		$this->_levelsControlsData = $this->_templater->getGenericBitmaskAndControls( WP_Plugin_FormHelper::wpytt_get_niveaux_kva(''), 'cbniveau' );

	}

	// Before rendering table to DB
	function setCss( $css ) {
		$this->_css = $css;
	}

	/**
	 * Initialisation avant rendu
	 * Consiste surtout en l'appel à templater->computeGaps
	 * qui permet de calculer les intervales entre chaque cours
	 */ 
	function renderInit() {
		/* Get intervals */
		list( $intervals, $breakHours, $mask, $bph ) = $this->_templater->computeGaps();
		$this->_intervals = $intervals;
		$this->_breakHours = $breakHours;
		$this->_intervalsMask = $mask;
		$this->_bitsPerHour = $bph;
	}

	/**
	 * Rendu de l'en-tête, incluant les contrôles et l'ouverture d'une div
	 */
	function renderHeader() {
		$out = $this->renderControls();
		$out .= '<div class="vtt-box">';
		return $out;
	}

	/**
	 * Rendu des "contrôles" du tableau : checkboxes permettant d'afficher/cacher les cours
	 * associés à tel ou tel prof, et les cours de tel ou tel niveau.
	 */
	function renderControls() {
		$controls = "\n\n<div class='tt-controls-row'><span class='cb-row-head'>Professeurs&nbsp;:</span>\n";
		$controls .= implode( "\n", $this->_teachersControlsData[1] );
		$teachersMaskAll = $this->_teachersControlsData[0]['__ALL__'];

		// On se servira de la valeur cachée #pmask dans le jQuery ou mootools qui contrôle l'interactivité
		$controls .= "<br><input type='hidden' id='pmask' name='pmask' value='$teachersMaskAll' /></div>\n" .
			"<div style='margin-top: 4px;' class='tt-controls-row'><span class='cb-row-head'>Niveaux&nbsp;:</span>\n";
		$controls .= implode( "\n", $this->_levelsControlsData[1] );
		$levelsMaskAll = $this->_levelsControlsData[0]['__ALL__'];

		// Idem pour #nmask
		$controls .= "<br><input type='hidden' id='nmask' name='nmask' value='$levelsMaskAll' /></div>\n";

		return $controls;
	}

	/**
	 * Effecuter le rendu de la 1ere colonne, avec les heures du jour.
	 */
	function renderHeadColumn() {

		// Les options heure_mini et heure_maxi sont stockées en minutes,
		// on les ramène en heures, et on calcule la durée du jour, 
		// en nombre d'heures
		$hmin = $this->_opts['heure_mini'] / 60;
		$hmax = $this->_opts['heure_maxi'] / 60;
		$span = $hmax - $hmin;

		// Index de l'intervalle
		// Un intervalle c'est un espace vide,
		// soit entre le début du jour et le 1er cours, soit entre chaque cours
		$idx_int = 0;

		// classe vday (v pour vertical, avant on pouvait aussi avoir h pour horizontal)
		// vday_narrow parce que la 1ere colonne sera plus étroite que les autres
		$hours_column = "<div class='vday vday_narrow'><!--\n" .
			"\t--><div class='vyclass colhead'>Heure</div><!--\n";

		for( $i = 0 ; $i < $span ; $i++ ) {
			// Façon terrible de tester si l'index de l'intervalle ($idx_int)
			// est supérieur ou égal à la taille du tableau $this->_intervals 
			// voir templater (wp-yoga-timetable-template.php) pour structure
			// d'un intervalle: tableau clés-valeurs avec clés: debut, fin, duree
			$intervalle = isset( $this->_intervals[$idx_int] ) ?
							$this->_intervals[$idx_int] : array();

			if( !empty( $intervalle ) && $i == $intervalle['debut'] ) {
				$sep =  ' vsep';
				$heure = $intervalle['debut'] . '-' . $intervalle['fin'] + $hmin;
				$idx_int++;
				$i += $intervalle['duree'] - 1;
			}
			else {
				$sep = '';
				$heure = $i + $hmin;
			}
			$hours_column .= "\t--><div class='vhour{$sep}'>&nbsp;$heure</div><!--\n";
		}

		$hours_column .= "--></div>";
		return $hours_column;
	}

	/**
	 * Calculer la marge entre deux cours:
	 * $class = classe courante, $previous = classe précédente,
	 * intervals = intervalles de pauses
	 */
	function computeMargin( $class, $previous, $intervals ) {
		// Offset par rapport à 0h, en minutes (ex: 540 pour 9h)
		$startDayMinutes = $this->_opts['heure_mini'];

		// Récupérer heure (en minutes) de fin de la classe précédente, si existe
		$previousClassEndMinutes = $previous != null ? $previous['endHour'] - $startDayMinutes : 0;
		// La convertir en heures
		$previousClassEndHours = floor( $previousClassEndMinutes / 60 );

		// Récupérer l'heure de départ de cette classe en minutes
		$thisClassBeginMinutes = $class['stHour'] - $startDayMinutes;
		// Convertir en heures
		$thisClassBeginHours = floor( $thisClassBeginMinutes / 60 );
		// Heure de fin en minutes
		$thisClassEndMinutes = $class['endHour'] - $startDayMinutes;
		// Heure de fin en heures
		$thisClassEndHours = floor( $thisClassEndMinutes / 60 );

		// Les intervalles de pause
		$numBreaks = 0; $numBreakHrs = 0;
		if( $intervals != null ) {
			// S'il y en a, on itere dessus.
			// Cette boucle permet de calculer, le cas échéant,
			// le nombre et la durée des pauses entre deux cours.
			// cela permet de calculer la marge en pixels ensuite
			foreach( $intervals as $i => $break ) {

				// Comparer heures des intervalles et heures de la classe
				// en comparant heures de debut et fin des deux

				if( $break['debut'] >= $thisClassBeginHours ) {
					break;
				}
				if( $break['fin'] <= $previousClassEndHours ) {
					break;
				}
				$numBreaks++;
				$numBreakHrs += $break['duree'];

			}
		}
		// Time interval between previous class and current class,
		// then substract break intervals total duration from this value
		$marginMinutes = $thisClassBeginMinutes - $previousClassEndMinutes - ( 60 * $numBreakHrs );
		// Add # of breaks * height of a break (sep_height) in pixels,
		// Add margin in pixels calculated from # of hours and pixels per hour (hour_height)
		$marginPx = $numBreaks * $this->_css['sep_height'] + ( $marginMinutes * $this->_css['hour_height'] ) / 60;
		// echo "nb: $numBreaks " . $marginMinutes .  ",$marginPx<br>";

		// substract 1 if > 0
		return $marginPx; // != 0 ? $marginPx - 1 : $marginPx;
	}

	/**
	 * Output a table column, corresponding to a day of the week
	 * @param day the day of week
	 */
	function renderColumn( $day ) {

		$dayPretty = $this->_days[$day];

		$output = "<div class='vday vday-{$day}'><!--\n"
			."--><div class='vyclass colhead'><b>{$dayPretty}</b></div><!--\n";

		$classesByDay = $this->_templater->getClasses();
		$days = $this->_templater->getDays();
		// Recupere cors pour ce jour
		$classes = $classesByDay[$day];
		$idx = 0;
		$previousClass = null;

		$classesHtml = [];
		// tout au long de la boucle on aura classData (données de cette classe) et previousClass (donnée classe précédente)
		foreach( $classes as $classSlug => $classData ) {
			// var_dump($classData);
			extract( $classData );
			// TODO: virer getTermNiveaux
			// getTermNiveaux returns ( $niveaux, $niveaux_str, $premier_niv_str, $slugs_str, $nivs_mask );
			list( $levels, $levelsString, $firstLevelString, $levelSlugs, $levelsMask ) = $this->_templater->getTermNiveaux( $id );
			$classData['levelsString'] = $levelsString;
			$classData['firstLevelString'] = $firstLevelString;

			// _teachersControlsData[0] = bitmasks des profs
			// $teacherSl = slug du prof dont on veut le bitmask
			$teachersMask = $this->_teachersControlsData[0][$teacherSl];

			// intervalles et # heures pauses
			$intervals = $this->_intervals;
			$breakHrs = $this->_breakHours;

			// Index = 0, 1er cours du jour, on calcule son "offset" en minutes par rapport au début du jour
			if( $idx === 0 ) {
				$marginMinutes = $stHour - $this->_opts['heure_mini'];
			}

			// Calcule de la durée en minutes du cours
			$duration = $endHour - $stHour;

			// Hauteur de la case du cours calculée en pixels par règle de 3 : 
			// hauteurs en pixels proportionnelle a la duree
			$heightPx = ( $duration * $this->_css['hour_height'] ) / 60 - ( $marginMinutes == 0 ? 1 : 0 );
			// Calculer marge en pixels entre deux cours, en tenant compte d'éventuelle(s) pause(s) entre les deux
			$marginPx = $this->computeMargin( $classData, $previousClass, $intervals );
			// if($marginPx < 0) {
			// 	var_dump($classData);die('overlaps');
			// }

			// Si il y a marge entre deux cours (cours ne sont pas collés), alors mettre ça dans le style "inline"
			$style = "margin-top: {$marginPx}px; height: {$heightPx}px;";
			// Si pas de marge, on ne met pas de bordure-top entre les deux (sachant qu'il y a déjà une bottom,
			// a la jonction entre deux cours cela ferait sinon une ligne de 2px, càd trop épaisse)
			$style .= $marginPx == 0 ? ' border-top: 0;' : '';
			$textStyle = "vertical-align: bottom;";

			// Si on n'a pas spécifié de modèle, prendre le "standard"
			if( empty( $model ) ) {
				$model = 'standard';
			}
			// récupérer le "template" de case en fonction du modèle
			$boxModel = $this->_templater->getBoxModel( $model );
			// var_dump($boxModel);
			// préparer un tableau pour fournir le paramétrage (les données) de la case
			$boxParams = array();
			foreach( $boxModel['args'] as $arg )
				$boxParams[] = $classData[$arg];
			$boxContent = vsprintf( $boxModel['string'], $boxParams );

			// Si on est admin on va remplacer la 1ère ligne (avec niveau et heure)
			// par un lien cliquable pour aller directement dans la partie admin éditer ce cours.
			// un bon raccourci pour modifier directement à partir d'une observation faite dans le tableau
			$isAdmin = current_user_can( 'manage_options' );
			$linkOpen = $isAdmin ? "<a target='_blank' href='" . get_edit_post_link( $id ) . "'>" : "";
			$linkClose = $isAdmin ? "</a>" : "";
			if( $model == 'custom' ) {
				$boxContent = str_replace( '$niv', $levelsString, $boxContent );
				//$fin = WP_Plugin_FormHelper::pretty_print_heure
				$boxContent = str_replace( '$debut', $stHourPr, $boxContent );
				$boxContent = str_replace( '$fin', $endHourPr, $boxContent );
				$boxContent = str_replace( '$prof', $teacherNm, $boxContent );
			}
			$boxContent = str_replace( '{linkOpen}', $linkOpen, $boxContent );
			$boxContent = str_replace( '{linkClose}', $linkClose, $boxContent );

			if($marginPx < 0) {
				$previousHtml = str_replace("style='margin-top:", "style='width: 50%;margin-top:", $classesHtml[$idx - 1]);
				$classesHtml[$idx - 1] = $previousHtml;
				$style = "width: 50%;margin-left:50%;$style";
			}

			// Ajouter cette case avec ses propriétés de style a l'output de la colonne
			// A noter: teacherSl et levelSlugs ça va par exemple être "luc-carimalo" et "n3 n4 n5"
			// On stocke le masque du prof et les masques de niveaux cumulés correspondant à ce cours
			// Ils seront manipulés par le JS.
			// Chaque checkbox de contrôle (ex. la checkbox correspondant à Yann) a un "poids", un "bitmask"
			// par exemple 1 pour Yann. Si le cours a pour prof associé Yann, son $teachersMask va valloir 1
			// On affiche la case du cours si la checkbox correspondant à son teachersMask est cochée
			$classesHtml[] = "--><div class='vyclass $teacherSl $levelSlugs' style='$style'"
				  . "><input type='hidden' value='$teachersMask|$levelsMask' /><div class='class-text'>"
				  . $boxContent
				  . "</div></div><!--";
			$previousClass = $classData;
			$idx++;
		}
		$output .= implode("\n", $classesHtml);
		$output .= "--></div>";
		return $output;
	}

	function renderIntervalsColumn() {
		$out = "<div class='vday vday-'><!--"
			 . "\t--><div class='vyclass colhead'>Mask</div><!--\n";

		//list( $intervals, $breakHrs, $mask, $bph ) = $this->_templater->computeGaps();
		$mask = $this->_intervalsMask;
		$bph = $this->_bitsPerHour;
		$bphm = $bph - 1;
		$bitHeightPx = floor( $this->_css['hour_height'] / $bph );
		$i = 0;
		foreach( $mask as $bit ) {
			$out .= $i % $bph == 0 ? "--><div class='vhour' style='border-bottom: 1px solid gray;'>" : "";
			$color = $bit ? 'green' : 'gray';
			$out .= "<div style='height: {$bitHeightPx}px; width: 100%; background: $color;'></div>" ;
			$out .= $i % $bph == $bphm ? "</div><!--" : "";
			$i++;
			}
		$out .= "--></div>";
		return $out;
	}

	/**
	 * Fermeture des div ouvertes précdemment
	 */
	function renderFooter() {
		return "</div>\n<!-- vtt-box -->\n";
	}

}
?>